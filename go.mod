module gitlab.com/securychat/securychat-server

go 1.23.5

require (
	github.com/google/go-tpm v0.9.3
	github.com/mattn/go-sqlite3 v1.14.22
)

require github.com/google/go-tpm-tools v0.4.4 // indirect

require (
	golang.org/x/crypto v0.32.0
	golang.org/x/sys v0.29.0 // indirect
)
