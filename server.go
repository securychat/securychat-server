/*
This file is part of SecuryChat.
Copyright (C) 2024 Laljaka

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"context"
	"fmt"
	"net"
	"strconv"
	"sync"

	"gitlab.com/securychat/securychat-server/db"
	"gitlab.com/securychat/securychat-server/encryption"
	"gitlab.com/securychat/securychat-server/protocol"
)

type Instruction byte

const (
	MAKE_PRIVATE_CHAT Instruction = iota
	ADD_ME            Instruction = iota
	NOT_ADD_ME        Instruction = iota
)

type ServerMSG struct {
	sender     db.UserID
	t          Instruction
	additional string
}

type Server struct {
	users    map[db.UserID]*MessageUser
	chats    map[db.ChatID]*Chat
	muUsers  sync.RWMutex
	muChats  sync.RWMutex
	db       db.Database
	verbose  bool
	bus      chan ServerMSG
	listener *net.TCPListener

	muTpm sync.Mutex
	tpm   *encryption.TPMAccesPoint
}

func (s *Server) GetPublicKey() ([]byte, error) {
	s.muTpm.Lock()
	defer s.muTpm.Unlock()
	return s.tpm.GetPublicKey()
}

func (s *Server) ValidateOrAdd(name string, hash string) (id db.UserID, passed bool, added bool, err error) {
	return s.db.ValidateOrAddUser(name, hash)
}

func (s *Server) Sign(challenge []byte) (signed []byte, err error) {
	s.muTpm.Lock()
	defer s.muTpm.Unlock()
	return s.tpm.Sign(challenge)
}

func (s *Server) NewChat(creator db.UserID, name string) (db.ChatID, error) {
	s.muChats.Lock()
	defer s.muChats.Unlock()

	id, err := s.db.AddChat(name)
	if err != nil {
		return 0, err
	}

	s.chats[id] = &Chat{
		users:   make(map[db.UserID]*MessageUser),
		invites: make(map[db.UserID]bool),
	}

	s.muUsers.RLock()
	defer s.muUsers.RUnlock()

	err = s.db.LinkUser(creator, id)
	if err != nil {
		return 0, err
	}

	s.chats[id].mu.Lock()
	defer s.chats[id].mu.Unlock()

	s.chats[id].users[creator] = s.users[creator]

	s.muUsers.RLock()
	defer s.muUsers.RUnlock()

	s.users[creator].AddChat(id, s.chats[id])
	fmt.Println(id)

	return id, nil
}

func (s *Server) InviteUserToChat(u db.UserID, c db.ChatID) {
	s.muChats.Lock() // mutexes might be unnecessary
	defer s.muChats.Unlock()
	s.muUsers.RLock()
	defer s.muUsers.RUnlock()

	s.chats[c].InviteUser(u)
	b := make([]byte, 1)
	b[0] = byte(c)
	s.users[u].Send(protocol.NewPayload(protocol.TUNNEL, b)) // random chat identifier
}

func (s *Server) AddUserToChat(u db.UserID, c db.ChatID) error {
	s.muChats.RLock()
	defer s.muChats.RUnlock()
	s.muUsers.RLock()
	defer s.muUsers.RUnlock()

	err := s.chats[c].AddUser(u, s.users[u])
	if err != nil {
		return err
	}

	err = s.db.LinkUser(u, c)
	if err != nil {
		s.chats[c].RemoveUser(u)
		return err
	}

	s.users[u].AddChat(c, s.chats[c])

	return nil
}

func (s *Server) DeleteUserFromChat(u db.UserID, c db.ChatID) error {
	s.muChats.RLock()
	defer s.muChats.RUnlock()
	s.muUsers.RLock()
	defer s.muUsers.RUnlock()

	err := s.db.UnlinkUser(u, c)
	if err != nil {
		return err
	}

	s.chats[c].RemoveUser(u)
	s.users[u].RemoveChat(c)

	return nil
}

func (s *Server) BusLoop(ctx context.Context, wg *sync.WaitGroup) {
	defer wg.Done()
	defer fmt.Println("return from bus reading")
	for {
		select {
		case smsg := <-s.bus:
			fmt.Println("Server msg", smsg)
			switch smsg.t {
			case MAKE_PRIVATE_CHAT:
				id, err := s.db.GetUserID(smsg.additional)
				if err != nil {
					fmt.Println(err)
					break
				}

				chat, err := s.NewChat(smsg.sender, fmt.Sprintf("Chat between %d and %d", smsg.sender, id))
				if err != nil {
					fmt.Println(err)
					break
				}

				s.InviteUserToChat(id, chat)
			case ADD_ME:
				id, err := strconv.ParseInt(smsg.additional, 10, 64) // USER SHOULD SOMEHOW KNOW HOW TO IDENTIFY THE CHAT
				if err != nil {
					fmt.Println(err)
					break
				}
				fmt.Println("addme")
				err = s.AddUserToChat(smsg.sender, db.ChatID(id))
				if err != nil {
					fmt.Println(err)
					break
				}
			case NOT_ADD_ME:
				fmt.Println("check")
				id, err := strconv.ParseInt(smsg.additional, 10, 64) // USER SHOULD SOMEHOW KNOW HOW TO IDENTIFY THE CHAT
				if err != nil {
					fmt.Println(err)
					break
				}

				s.muChats.RLock()
				s.chats[db.ChatID(id)].mu.Lock()
				_, ok := s.chats[db.ChatID(id)].invites[smsg.sender]
				if ok {
					s.chats[db.ChatID(id)].invites[smsg.sender] = false
				}
				s.chats[db.ChatID(id)].mu.Unlock()
				s.muChats.RUnlock()
			}
		case <-ctx.Done():
			fmt.Println("receiver done")
			return
		}
	}
}

func (s *Server) GetUser(id db.UserID) *MessageUser {
	s.muUsers.RLock()
	defer s.muUsers.RUnlock()
	usr, ok := s.users[id]
	if ok {
		return usr
	} else {
		panic("This should never happen")
	}
}

/*
func (s *Server) SendMessageTo(u db.UserID, m Message) { // Will later distinguish if should be sent or saved

		s.mutexConnections.RLock()
		ref, ok := s.connections[u]
		if ok {
			go func(ch chan Message, msg Message) { // UNSAFE NEEDS CHANGE maybe channel should be on user, not connection, and decide on the other end what to do with the message?
				ch <- msg
			}(ref.inbox, m)
		}
		s.mutexConnections.RUnlock()
	}

	func (s *Server) AskForChat(u db.UserID) {
		s.mutexConnections.RLock()
		_, ok := s.connections[u]
		if ok {

		}
		s.mutexConnections.RUnlock()
	}
*/
func (s *Server) PrintStats() {
	s.muUsers.RLock()
	fmt.Println(s.users)
	s.muUsers.RUnlock()
}
