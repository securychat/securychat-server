/*
This file is part of SecuryChat.
Copyright (C) 2024 Laljaka

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"errors"
	"sync"

	"gitlab.com/securychat/securychat-server/db"
	"gitlab.com/securychat/securychat-server/protocol"
)

/*
import (
	"sync"
)

func NewChatFullErr() error {
	return &ChatFullErr{"Chat is full"}
}

type ChatFullErr struct {
	s string
}

func (err *ChatFullErr) Error() string {
	return err.s
}

func NewPrivateChat() *PrivChat {
	return &PrivChat{subs: make(map[string]chan<- PublicMessage, 2)}
}

type PrivChat struct {
	id    string
	subs  map[string]chan<- PublicMessage
	msubs sync.RWMutex
}

func (c *PrivChat) subscribe(author string, ch chan<- PublicMessage) error {
	if len(c.subs) >= 2 {
		return NewChatFullErr()
	}
	c.msubs.Lock()
	c.subs[author] = ch
	c.msubs.Unlock()
	return nil
}

func (c *PrivChat) publish(author string, m []byte) error {
	c.msubs.RLock()
	defer c.msubs.RUnlock()
	for sub, ch := range c.subs {
		if sub != author {
			ch <- PublicMessage{chat: c.id, data: m}
		}
	}
	return nil
}

type PubChat struct {
	id    string
	subs  map[string]chan<- PublicMessage
	msubs sync.RWMutex
}

func (c *PubChat) subscribe(author string, ch chan<- PublicMessage) error {
	c.msubs.Lock()
	c.subs[author] = ch
	c.msubs.Unlock()
	return nil
}

func (c *PubChat) publish(author string, m []byte) error {
	c.msubs.RLock()
	for sub, ch := range c.subs {
		if sub != author {
			ch <- PublicMessage{chat: c.id, data: m}
		}
	}
	c.msubs.RUnlock()
	return nil
}

type Chat interface {
	subscribe(author string, ch chan<- PublicMessage) error
	publish(author string, m []byte) error
}

*/

type Chat struct {
	mu      sync.RWMutex
	users   map[db.UserID]*MessageUser
	invites map[db.UserID]bool
}

func (c *Chat) Publish(sender db.UserID, m protocol.Payload) {
	c.mu.RLock()
	defer c.mu.RUnlock()
	for usr, ref := range c.users {
		if usr != sender {
			ref.Send(m)
		}
	}
}

func (c *Chat) InviteUser(u db.UserID) {
	c.mu.Lock()
	c.invites[u] = true
	c.mu.Unlock()
}

func (c *Chat) NoToInvite(u db.UserID) {
	c.mu.Lock()
	c.invites[u] = false
	c.mu.Unlock()
}

func (c *Chat) AddUser(u db.UserID, usr *MessageUser) error {
	c.mu.Lock()
	defer c.mu.Unlock()
	if len(c.users) >= 2 {
		return errors.New("chat is full")
	}
	ko, ok := c.invites[u]
	if !ok || !ko {
		return errors.New("user was not invited to the chat or declined an invitation earlier")
	}
	delete(c.invites, u)
	c.users[u] = usr
	// TELL THE SERVER USER HAS JOINED c.incoming <- MUserToChat{sender: 0, payload: []byte("USER JOINED CHAT")} // THIS SHOULD SPECIFY WHO JOINED
	return nil
}

func (c *Chat) RemoveUser(u db.UserID) {
	c.mu.Lock()
	defer c.mu.Unlock()
	delete(c.users, u)
	// TELL SERVER USER LEFT c.incoming <- MUserToChat{sender: 0, payload: []byte("USER LEFT CHAT")} // THIS SHOULD SPECIFY WHO LEFT
}
