/*
This file is part of SecuryChat.
Copyright (C) 2024 Laljaka

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package main

import "fmt"

func drainChannelWithoutClose[T any](ch <-chan T) {
	for {
		select {
		case val := <-ch:
			fmt.Println("Drained value:", val)
		default: // Exit when no more values are available
			fmt.Println("Channel is empty now.")
			return
		}
	}
}

func inSlice[T comparable](a T, slice []T) bool {
	for _, b := range slice {
		if b == a {
			return true
		}
	}
	return false
}

func mapStringsToBA[T comparable](m map[T]string) []byte {
	res := make([]byte, 0)
	for _, key := range m {
		res = append(res, key...)
		res = append(res, ","...)
	}
	return res
}

func byteArrayBuilder(bytes ...byte) []byte {
	return bytes
}

func byteBuilder(bytes ...byte) byte {
	var final byte
	for _, b := range bytes {
		final = final | b
	}
	return final
}
