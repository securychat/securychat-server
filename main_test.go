/*
This file is part of SecuryChat.
Copyright (C) 2024 Laljaka

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"context"
	"crypto"
	"crypto/rsa"
	"crypto/x509"
	"encoding/binary"
	"encoding/hex"
	"errors"
	"fmt"
	"net"
	"os"
	"sync"
	"testing"
	"time"

	"gitlab.com/securychat/securychat-server/protocol"
)

var pu string

func checkRSASign(toCheck []byte, pub []byte, sign []byte) error {
	akhsh := crypto.SHA256.New()
	akhsh.Write(toCheck)

	decoded, err := decodeRSA(pub)
	if err != nil {
		return err
	}

	if err := rsa.VerifyPKCS1v15(decoded, crypto.SHA256, akhsh.Sum(nil), sign); err != nil {
		return errors.Join(errors.New("could not verify"), err)
	}

	return nil
}

func decodeRSA(pubKey []byte) (*rsa.PublicKey, error) {
	der, err := x509.ParsePKIXPublicKey(pubKey)
	if err != nil {
		return nil, err
	}

	der2, ok := der.(*rsa.PublicKey)
	if !ok {
		return nil, errors.New("oops")
	}

	return der2, nil
}

func handshake(remote *net.TCPAddr, port int, user string, password string, pubkeyserv string) (*protocol.Connection, error) {
	laddr := &net.TCPAddr{IP: net.ParseIP("localhost"), Port: port}

	conn, err := net.DialTCP("tcp", laddr, remote)
	if err != nil {
		return nil, err
	}

	c, err := protocol.UpgradeConnection(conn, false)
	if err != nil {
		return nil, err
	}

	if pubkeyserv == "" {
		err = c.Write(protocol.NewPayload(protocol.PUBKEY, nil))
		if err != nil {
			return nil, err
		}
		recv, err := c.Receive()
		if err != nil {
			return nil, err
		}
		fmt.Println(recv.Opcode, hex.EncodeToString(recv.Data))
		pu = hex.EncodeToString(recv.Data)
	} else {
		dec, err := hex.DecodeString(pubkeyserv)
		if err != nil {
			return nil, err
		}

		challenge := "this is a test"

		err = c.Write(protocol.NewPayload(protocol.PUBKEY, []byte(challenge)))
		if err != nil {
			return nil, err
		}
		recv, err := c.Receive()
		if err != nil {
			return nil, err
		}

		fmt.Println(recv.Opcode, hex.EncodeToString(recv.Data))

		err = checkRSASign([]byte(challenge), dec, recv.Data)
		if err != nil {
			return nil, err
		}
	}

	recv, err := c.Receive()
	if err != nil {
		return nil, err
	}
	if recv.Opcode != protocol.LIST {
		return nil, err
	}
	fmt.Println(string(recv.Data))

	payload := make([]byte, 2)
	binary.BigEndian.PutUint16(payload[0:2], uint16(len(user)))
	payload = append(payload, user...)
	payload = append(payload, password...)

	err = c.Write(protocol.NewPayload(protocol.REQUEST, payload))
	if err != nil {
		return nil, err
	}

	recv, err = c.Receive()
	if err != nil {
		return nil, err
	}
	if recv.Opcode != protocol.LIST {
		return nil, err
	}
	fmt.Println(string(recv.Data))

	return c, nil
}

func TestMain(m *testing.M) {
	os.Remove("fulltest.db")
	context, cancel := context.WithCancel(context.Background())
	defer cancel()
	var control sync.WaitGroup

	control.Add(1)
	go Run(context, true, &control, "fulltest")

	control.Wait()

	code := m.Run()

	cancel()

	time.Sleep(time.Second * 1)

	os.Exit(code)
}

func TestChat(t *testing.T) {
	addr := &net.TCPAddr{IP: net.ParseIP("localhost"), Port: 3939}
	conn, err := handshake(addr, 3777, "Laljaka", "VerySecurePassword123", pu)
	if err != nil {
		t.Fatal(err)
	}

	addr2 := &net.TCPAddr{IP: net.ParseIP("localhost"), Port: 3939}
	conn2, err := handshake(addr2, 1115, "Tom", "Pass2", pu)
	if err != nil {
		t.Fatal(err)
	}

	t.Cleanup(func() {
		conn.Close()
		conn2.Close()
	})

	err = conn2.Write(protocol.NewPayload(protocol.TUNNEL, []byte("Laljaka")))
	if err != nil {
		t.Fatal(err)
	}

	recv, err := conn.Receive()
	if err != nil {
		t.Fatal(err)
	}
	if recv.Opcode != protocol.TUNNEL {
		t.Fatal(recv.Opcode)
	}

	ans := append(recv.Data, 1)

	err = conn.Write(protocol.NewPayload(protocol.ANSWER, ans))
	if err != nil {
		t.Fatal(err)
	}

	time.Sleep(time.Second * 1) // THIS SHOULD BE A CONFIRMATION FROM THE SERVER

	msg1 := "Hello"
	msg2 := "Hi"

	t.Log("passed creation", recv.Data)

	err = conn.Write(protocol.NewPayload(protocol.MESSAGE, append(recv.Data, append([]byte("Laljaka"), msg1...)...)))
	if err != nil {
		t.Fatal(err)
	}

	err = conn2.Write(protocol.NewPayload(protocol.MESSAGE, append(recv.Data, append([]byte("Tom"), msg2...)...)))
	if err != nil {
		t.Fatal(err)
	}

	recv, err = conn2.Receive()
	if err != nil {
		t.Fatal(err)
	}
	if recv.Opcode != protocol.MESSAGE {
		t.Fatal(recv.Opcode)
	}
	t.Log(string(recv.Data))

	recv, err = conn.Receive()
	if err != nil {
		t.Fatal(err)
	}
	if recv.Opcode != protocol.MESSAGE {
		t.Fatal(recv.Opcode)
	}
	t.Log(string(recv.Data))

}
