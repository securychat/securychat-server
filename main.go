/*
This file is part of SecuryChat.
Copyright (C) 2024 Laljaka

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"context"
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"net"
	"sync"

	"gitlab.com/securychat/securychat-server/db"
	"gitlab.com/securychat/securychat-server/encryption"
	"gitlab.com/securychat/securychat-server/linked_list"
	"gitlab.com/securychat/securychat-server/protocol"

	_ "github.com/mattn/go-sqlite3"
)

func (s *Server) listen(pctx context.Context, pwg *sync.WaitGroup) {
	defer fmt.Println("listener done")
	fmt.Println("Server is running")

	for {
		uconn, err := s.listener.AcceptTCP()
		if err != nil {
			if errors.Is(err, net.ErrClosed) {
				break
			}
			fmt.Println(err)
			continue
		}

		pwg.Add(1)
		go s.handle(pctx, pwg, uconn)
	}
}

func (s *Server) handle(pctx context.Context, pwg *sync.WaitGroup, uconn *net.TCPConn) {
	defer pwg.Done()
	defer fmt.Println(uconn.RemoteAddr(), "closed")
	defer uconn.Close()
	err := uconn.SetKeepAlive(false)
	if err != nil {
		fmt.Println(err)
		return
	}
	//----------------------------------HS-------------------------------
	fmt.Println("New conn from", uconn.RemoteAddr(), "shaking hands...")

	// Upgrading connection ---------------------------------------------------------------------------------------------------------------
	cc, err := protocol.UpgradeConnection(uconn, true)
	if err != nil {
		fmt.Println(err)
		return
	}

	c := &Connection2{
		Connection: cc,
		sbus:       s.bus,
	}

	// Checking if the client is new or old -----------------------------------------------------------------------------------------------------------------
	recv, err := c.Receive()
	if err != nil {
		fmt.Println(err)
		return
	}
	if recv.Opcode != protocol.PUBKEY {
		fmt.Println("not allowed")
		return
	}

	fmt.Println("before")
	var payload []byte
	if recv.IsData() {
		fmt.Println("branch")
		payload, err = s.Sign(recv.Data)
		if err != nil {
			fmt.Println(err)
			return
		}
	} else {
		payload, err = s.GetPublicKey()

		if err != nil {
			fmt.Println(err)
			return
		}
	}
	fmt.Println("after")

	err = c.Write(protocol.NewPayload(protocol.PUBKEY, payload))
	if err != nil {
		fmt.Println(err)
		return
	}

	users, err := s.db.GetAllUsers()
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(users)

	marshalled, err := json.Marshal(users)
	if err != nil {
		fmt.Println(err)
		return
	}

	err = c.Write(protocol.NewPayload(protocol.LIST, marshalled))
	if err != nil {
		fmt.Println(err)
		return
	}

	recv, err = c.Receive()
	if err != nil {
		fmt.Println(err)
		return
	}
	if recv.Opcode != protocol.REQUEST || !recv.IsData() {
		fmt.Println("Unsupported handshake")
		return
	}

	fmt.Println(string(recv.Data[2:]))

	nameLen := binary.BigEndian.Uint16(recv.Data[0:2])
	name := string(recv.Data[2 : nameLen+2])
	hash := string(recv.Data[nameLen+2:])

	id, passed, added, err := s.ValidateOrAdd(name, hash)
	if err != nil {
		fmt.Println("ABORTING")
		return
	}
	if !passed && !added {
		fmt.Println("ABORTING")
		return
	}

	ch := make(chan protocol.Payload)

	var usr *MessageUser
	if added {
		usr = &MessageUser{
			me:      id,
			list:    linked_list.LinkedList[protocol.Payload]{},
			allowed: make(map[db.ChatID]*Chat),
		}

		//usr.Online(ch)

		s.muUsers.Lock()
		s.users[id] = usr
		s.muUsers.Unlock()
	} else {
		s.muUsers.RLock()
		usr = s.users[id]
		//usr.Online(ch)
		s.muUsers.RUnlock()
	}
	//defer usr.Offline()
	//---------------------------HS over----------------------------

	//ctx, cancel := context.WithCancel(pctx)
	//defer cancel()
	c.owner = id
	c.oRef = usr
	c.ch = ch

	usr.mu.RLock()
	dt := make([]db.ChatID, len(usr.allowed))
	i := 0
	for c := range usr.allowed {
		dt[i] = c
		i++
	}
	pl2, err := json.Marshal(dt)
	usr.mu.RUnlock()
	if err != nil {
		fmt.Println(err)
		return
	}

	err = c.Write(protocol.NewPayload(protocol.LIST, pl2))
	if err != nil {
		fmt.Println(err)
		return
	}

	usr.Online(ch)
	usr = nil

	closech := make(chan interface{})

	pwg.Add(1)
	go c.handleSend(pctx, pwg, closech)

	c.handleRecv(closech)
}

/*
	func (connection *Connection) handleSend(ctx context.Context, wg *sync.WaitGroup, privch <-chan protocol.Message, pubch <-chan DispatcherMessage) {
		defer wg.Done()
		select {
		case one := <-privch:
			_, err := connection.uconn.Write(one.Build())
			if err != nil {
				fmt.Println(err)
			}
		case two := <-pubch:
			if inSlice(two.chat, connection.subscribedChats) {
				_, err := connection.uconn.Write(protocol.NewMessage(protocol.MESSAGE, two.data).Build())
				if err != nil {
					fmt.Println(err)
				}
			} else {
				fmt.Println("not allowed")
			}
		case <-ctx.Done():
			return
		}
	}
*/

func main() {
	ctx := context.Background()
	Run(ctx, false, nil, "securychat")
}

func Run(pctx context.Context, testing bool, control *sync.WaitGroup, dbname string) {
	d, err := db.CreateSqliteDB(dbname)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer d.Close()

	var handle uint32
	handle, err = d.GetHandle()
	if err != nil {
		fmt.Println(err)
		return
	}
	device, err := encryption.GetTPMDevice()
	if err != nil {
		fmt.Println(err)
		return
	}

	if handle == 0 { //TODO MAKE SURE THAT SERVER NEVER RESETS THIS WITHOUT NOTIFICATION
		fmt.Println("Handle generating")
		handle, err = encryption.GeneratePersistantHandle(device)
		if err != nil {
			fmt.Println(err)
			return
		}
	}

	err = d.Prepare()
	if err != nil {
		fmt.Println(err)
		return
	}

	addr := &net.TCPAddr{IP: net.ParseIP("localhost"), Port: 3939}
	listener, err := net.ListenTCP("tcp", addr) //"localhost:3939")
	if err != nil {
		return
	}

	var wg sync.WaitGroup

	ctx, cancel := context.WithCancel(pctx)

	defer func() {
		cancel()
		listener.Close()
		wg.Wait()
		fmt.Println("Exited gracefully.")
	}()

	ch := make(chan ServerMSG, 10)

	users, err := d.GetAllUsers()
	if err != nil {
		fmt.Println(err)
		return
	}

	chats, err := d.GetAllChats()
	if err != nil {
		fmt.Println(err)
		return
	}

	tpm := encryption.NewTPMAccesPoint(device)
	tpm.SetHandle(handle)

	server := Server{
		users:    make(map[db.UserID]*MessageUser),
		chats:    make(map[db.ChatID]*Chat),
		db:       d,
		verbose:  !testing,
		bus:      ch,
		listener: listener,
		tpm:      tpm,
	}

	for id := range users {
		server.users[id] = &MessageUser{
			me:      id,
			list:    linked_list.LinkedList[protocol.Payload]{},
			allowed: make(map[db.ChatID]*Chat),
		}
		server.users[id].Offline()
	}

	for chat, user := range chats {
		server.chats[chat] = &Chat{
			users:   make(map[db.UserID]*MessageUser),
			invites: make(map[db.UserID]bool),
		}
		for _, usr := range user {
			server.chats[chat].users[usr] = server.users[usr]
			server.users[usr].allowed[chat] = server.chats[chat]
		}
	}

	wg.Add(1)
	go server.BusLoop(ctx, &wg)

	wg.Add(1)
	go server.listen(ctx, &wg)

	if !testing {
	out:
		for {
			var input string
			fmt.Scanln(&input)
			switch input {
			case "quit":
				break out
			case "stat":
				server.PrintStats()
			}
		}
	} else {
		control.Done()
		wg.Wait()
	}
}
