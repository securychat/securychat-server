/*
This file is part of SecuryChat.
Copyright (C) 2024 Laljaka

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package protocol

import (
	"encoding/binary"
	"errors"
	"io"
	"net"
)

type receiver struct {
	head [2]byte
}

func newReceiver() *receiver {
	return &receiver{}
}

func (r *receiver) receiveFull(conn net.Conn) ([]byte, error) {
	_, err := io.ReadFull(conn, r.head[:])
	if err != nil {
		return nil, err
	}

	size := binary.BigEndian.Uint16(r.head[:])
	if size > uint16(MAX_PAYLOAD_SIZE) {
		return nil, errors.New("too much")
	}
	if size <= 2 {
		return nil, errors.New("not allowed size")
	}

	got := make([]byte, size)
	_, err = io.ReadFull(conn, got)
	if err != nil {
		return nil, err
	}

	return got, nil
}
