/*
This file is part of SecuryChat.
Copyright (C) 2024 Laljaka

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package protocol

import (
	"errors"
	"io"
	"net"
)

type receiver struct {
	head [HEAD_SIZE]byte
}

func newReceiver() *receiver {
	return &receiver{}
}

func (r *receiver) receivePacket(conn net.Conn) (header, []byte, error) {
	_, err := io.ReadFull(conn, r.head[:])
	if err != nil {
		return header{}, nil, err
	}

	final, version, code, length, err := parseHeader(r.head[:])
	if err != nil {
		return header{}, nil, err
	}
	if length > uint16(MAX_PACKET_SIZE-HEAD_SIZE) {
		return header{}, nil, errors.New("length is too long")
	}
	if !final && length != uint16(MAX_PACKET_SIZE-HEAD_SIZE) {
		return header{}, nil, errors.New("not final packet can only be with full length")
	}

	data := make([]byte, length)
	_, err = io.ReadFull(conn, data)
	if err != nil {
		return header{}, nil, err
	}

	return header{
		code:       code,
		version:    version,
		final:      final,
		payloadLen: uint16(length),
	}, data, nil
}
