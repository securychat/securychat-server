/*
This file is part of SecuryChat.
Copyright (C) 2024 Laljaka

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package protocol

import (
	"errors"
	"net"
)

type sender struct {
	data  [MAX_PACKET_SIZE]byte
	empty [MAX_PACKET_SIZE]byte
	last  int
}

func newSender() *sender {
	return &sender{}
}

func (b *sender) clear() {
	copy(b.data[:b.last], b.empty[:b.last])
	b.last = 0
}

func (b *sender) getSlice(length int) ([]byte, error) {
	if length+b.last > MAX_PACKET_SIZE {
		return nil, errors.New("size is too much")
	}

	cur := b.last
	b.last += length

	return b.data[cur:b.last], nil
}

func (b *sender) flush(conn net.Conn) (err error) {
	defer b.clear()
	_, err = conn.Write(b.data[:b.last])
	return
}
