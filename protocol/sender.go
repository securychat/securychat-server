/*
This file is part of SecuryChat.
Copyright (C) 2024 Laljaka

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package protocol

import (
	"encoding/binary"
	"errors"
)

type sender struct {
	data  [MAX_PAYLOAD_SIZE]byte
	empty [MAX_PAYLOAD_SIZE]byte
	last  int
}

func newSender() *sender {
	return &sender{}
}

func (b *sender) zeroOut(howmany int) {
	copy(b.data[:howmany], b.empty[:howmany])
}

func (b *sender) getSlice(length int) ([]byte, error) {
	var size int = 2

	if size+length > MAX_PAYLOAD_SIZE {
		return nil, errors.New("size is too much")
	}

	b.zeroOut(b.last)
	b.last = size + length

	binary.BigEndian.PutUint16(b.data[:2], uint16(length))

	return b.data[size : length+size], nil
}

func (b *sender) getFullSlice() []byte {
	return b.data[:b.last]
}
