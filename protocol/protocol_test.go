/*
This file is part of SecuryChat.
Copyright (C) 2024 Laljaka

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package protocol

import (
	"crypto/rand"
	"fmt"
	"net"
	"testing"

	"gitlab.com/securychat/securychat-server/encryption"
)

func BenchmarkBuildFinalApproach1(b *testing.B) {
	in, out := net.Pipe()
	defer in.Close()
	defer out.Close()

	builder := newSender()
	payload := make([]byte, 100)

	got := make([]byte, 1024)
	go func(sl []byte, o net.Conn) {
		for {
			_, err := o.Read(sl)
			if err != nil {
				return
			}
		}
	}(got, out)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		slice, err := builder.getSlice(len(payload))
		if err != nil {
			b.Fatal(err)
		}
		copy(slice, payload)

		builder.flush(in)
	}
}

func TestIntegration(t *testing.T) {
	in, out := net.Pipe()
	defer in.Close()
	defer out.Close()
	key := make([]byte, 32)
	_, err := rand.Read(key)
	if err != nil {
		t.Fatal(err)
	}

	aead, err := encryption.NewAESGCM(key)
	if err != nil {
		t.Fatal(err)
	}

	pl := NewPayload(ECDH, []byte("12345"))
	fmt.Println(pl)

	nonce := make([]byte, aead.NonceSize())
	_, err = rand.Read(nonce)
	if err != nil {
		t.Fatal(err)
	}

	b := newSender()

	slice, err := b.getSlice(HEAD_SIZE)
	if err != nil {
		t.Fatal(err)
	}
	constructHeader(slice, pl.opcode, true, 5+12+16)
	fmt.Println(slice)

	slice2, err := b.getSlice(12)
	if err != nil {
		t.Fatal(err)
	}

	copy(slice2, nonce)
	fmt.Println(slice2)

	slice3, err := b.getSlice(5 + 16)
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(slice3)

	aead.Seal(slice3[:0], nonce, pl.body, slice)

	fmt.Println(len(slice3))
	fmt.Println(slice3)

	go b.flush(in)

	r := newReceiver()

	head, got, err := r.receivePacket(out)
	if err != nil {
		t.Fatal(err)
	}

	fmt.Println(head)
	fmt.Println(len(got))
	fmt.Println(got)

	pt, err := aead.Open(nil, got[:12], got[12:], r.head[:])
	if err != nil {
		t.Fatal(err)
	}

	t.Log(pt)
}

func TestFinal(t *testing.T) {
	addr, err := net.ResolveTCPAddr("tcp", "localhost:1228")
	if err != nil {
		t.Fatal(err)
	}
	t.Run("in conn", func(t *testing.T) {
		t.Parallel()

		list, err := net.ListenTCP("tcp", addr)
		if err != nil {
			t.Fatal(err)
		}

		tcp, err := list.AcceptTCP()
		if err != nil {
			t.Fatal(err)
		}

		newConn, err := UpgradeConnection(tcp, true)
		if err != nil {
			t.Fatal(err)
		}
		t.Log("upgraded in")

		var data []byte

		err = newConn.Write(NewPayload(MESSAGE, data))
		if err != nil {
			t.Fatal(err)
		}

		recv, err := newConn.Receive()
		if err != nil {
			t.Fatal(err)
		}
		t.Log(recv)
	})

	t.Run("out conn", func(t *testing.T) {
		t.Parallel()

		tcp, err := net.DialTCP("tcp", nil, addr)
		if err != nil {
			t.Fatal(err)
		}

		newConn, err := UpgradeConnection(tcp, false)
		if err != nil {
			t.Fatal(err)
		}
		t.Log("upgraded out")

		var data []byte

		err = newConn.Write(NewPayload(MESSAGE, data))
		if err != nil {
			t.Fatal(err)
		}

		recv, err := newConn.Receive()
		if err != nil {
			t.Fatal(err)
		}
		t.Log(recv)
	})
}
