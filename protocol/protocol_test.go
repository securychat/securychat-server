/*
This file is part of SecuryChat.
Copyright (C) 2024 Laljaka

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package protocol

import (
	"crypto/rand"
	"fmt"
	"net"
	"testing"

	"gitlab.com/securychat/securychat-server/encryption"
)

func BenchmarkBuildFinalApproach1(b *testing.B) {
	in, out := net.Pipe()
	defer in.Close()
	defer out.Close()

	builder := newSender()
	payload := make([]byte, 100)
	nonce := make([]byte, 12)
	add := make([]byte, 16)

	got := make([]byte, 1024)
	go func(sl []byte, o net.Conn) {
		for {
			_, err := o.Read(sl)
			if err != nil {
				return
			}
		}
	}(got, out)

	for i := 0; i < b.N; i++ {
		slice, err := builder.getSlice(len(payload) + 12 + 16)
		if err != nil {
			b.Fatal(err)
		}
		copy(slice, nonce)
		copy(slice[12:], payload)
		copy(slice[12+len(payload):], add)
		in.Write(builder.getFullSlice())
	}
}

func BenchmarkPayloadToBytesApproach2(b *testing.B) {
	payload := Payload{opcode: MESSAGE, body: make([]byte, 40000)}
	var res []byte
	for i := 0; i < b.N; i++ {
		res = payload.encode()
	}
	_ = res
}

func TestIntegration(t *testing.T) {
	in, out := net.Pipe()
	defer in.Close()
	defer out.Close()
	key := make([]byte, 32)
	_, err := rand.Read(key)
	if err != nil {
		t.Fatal(err)
	}

	aead, err := encryption.NewAESGCM(key)
	if err != nil {
		t.Fatal(err)
	}

	pl := NewPayload(ECDH, []byte("12345")).encode()
	fmt.Println(len(pl))

	nonce := make([]byte, aead.NonceSize())
	_, err = rand.Read(nonce)
	if err != nil {
		t.Fatal(err)
	}

	b := newSender()

	slice, err := b.getSlice(len(pl) + 12 + 16)
	if err != nil {
		t.Fatal(err)
	}
	fmt.Println(slice)

	copy(slice, nonce)
	fmt.Println(slice)

	aead.Seal(slice[12:12], nonce, pl, nil)

	fmt.Println(len(slice))
	fmt.Println(slice)

	fullslice := b.getFullSlice()
	fmt.Println(fullslice)
	go in.Write(fullslice)

	r := newReceiver()

	got, err := r.receiveFull(out)
	if err != nil {
		t.Fatal(err)
	}

	fmt.Println(len(got))
	fmt.Println(got)

	pt, err := aead.Open(nil, got[:12], got[12:], nil)
	if err != nil {
		t.Fatal(err)
	}

	re, err := parse(pt)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(re)
}
