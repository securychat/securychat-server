/*
This file is part of SecuryChat.
Copyright (C) 2024 Laljaka

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package protocol

import (
	"errors"
)

type VER byte

const (
	VERSION           VER = 0b000
	MAX_PAYLOAD_SIZE  int = 0xFFFF
	SOFT_PAYLOAD_SIZE int = MAX_PAYLOAD_SIZE - 30
)

// OPCODES:

type OPCODE byte

const (
	REQUEST OPCODE = iota
	LIST    OPCODE = iota
	ECDH    OPCODE = iota
	CLOSE   OPCODE = iota
	MESSAGE OPCODE = iota // TEMP FOR TESTING
	TUNNEL  OPCODE = iota
	PUBKEY  OPCODE = iota // from client with bit 1 - asking for key, bit 0 - no need
	ANSWER  OPCODE = iota
)

func (code OPCODE) String() string {
	switch code {
	case REQUEST:
		return "REQUEST"
	case LIST:
		return "LIST"
	case CLOSE:
		return "CLOSE"
	case MESSAGE:
		return "MESSAGE"
	case ECDH:
		return "ECDH"
	case TUNNEL:
		return "TUNNEL"
	case PUBKEY:
		return "PUBKEY"
	case ANSWER:
		return "ANSWER"
	default:
		return "NOT ALLOWED"
	}
}

type Payload struct {
	opcode OPCODE
	body   []byte
}

type received struct {
	Ver    VER
	Opcode OPCODE
	IsData bool
	Data   []byte
}

// To ensure safety all the data past (max uint16 value - 30) bytes will be cut and only first (max uint16 value - 30)
// bytes will be put
// into a payload. Limits might and probably will be increased in future.
func NewPayload(opcode OPCODE, body []byte) Payload {
	if len(body) > SOFT_PAYLOAD_SIZE {
		body = body[:SOFT_PAYLOAD_SIZE]
	}
	return Payload{opcode: opcode, body: body}
}

func (m Payload) encode() []byte {
	out := make([]byte, 1+4, len(m.body)+1+4)

	out[0] = byte((VERSION << 5)) | byte(m.opcode)

	if m.body != nil {
		out = append(out, m.body...)
	}

	return out
}

func parseHeader(head []byte) (VER, OPCODE, error) {
	if len(head) != 1+4 {
		return 0, 0, errors.New("HEAD SIZE TOO BIG")
	}
	version := VER(head[0] >> 5)
	if version != VERSION {
		return 0, 0, errors.New("VERSION NOT ALLOWED")
	}

	opcode := OPCODE(head[0] & 0b0001_1111)
	if opcode > ANSWER || opcode < REQUEST {
		return 0, 0, errors.New("OPCODE OUT OF RANGE")
	}

	return version, opcode, nil
}

func parse(data []byte) (received, error) {
	version, code, err := parseHeader(data[:5])
	if err != nil {
		return received{}, err
	}

	//len := int32(((header[1] << (8 + 8 + 8)) | (header[2] << (8 + 8)) | (header[3] << 8) | header[4]))

	if len(data) > 5 {
		return received{
			Ver:    version,
			Opcode: code,
			IsData: true,
			Data:   data[5:],
		}, nil
	} else {
		return received{
			Ver:    version,
			Opcode: code,
			IsData: false,
			Data:   nil,
		}, nil
	}
}
