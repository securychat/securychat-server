/*
This file is part of SecuryChat.
Copyright (C) 2024 Laljaka

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package protocol

import (
	"errors"
)

type VER byte

const (
	VERSION           VER = 0b0000
	HEAD_SIZE         int = 3
	AUTH_TAG          int = 16
	MAX_PACKET_SIZE   int = 1440
	SOFT_PAYLOAD_SIZE int = MAX_PACKET_SIZE - HEAD_SIZE - AUTH_TAG
)

// OPCODES:

type OPCODE byte

const (
	REQUEST OPCODE = iota
	LIST    OPCODE = iota
	ECDH    OPCODE = iota
	CLOSE   OPCODE = iota
	MESSAGE OPCODE = iota // TEMP FOR TESTING
	TUNNEL  OPCODE = iota
	PUBKEY  OPCODE = iota // from client with bit 1 - asking for key, bit 0 - no need
	ANSWER  OPCODE = iota
)

func (code OPCODE) String() string {
	switch code {
	case REQUEST:
		return "REQUEST"
	case LIST:
		return "LIST"
	case CLOSE:
		return "CLOSE"
	case MESSAGE:
		return "MESSAGE"
	case ECDH:
		return "ECDH"
	case TUNNEL:
		return "TUNNEL"
	case PUBKEY:
		return "PUBKEY"
	case ANSWER:
		return "ANSWER"
	default:
		return "NOT ALLOWED"
	}
}

type Payload struct {
	opcode OPCODE
	body   []byte
}

type received struct {
	Ver    VER
	Opcode OPCODE
	Data   []byte
}

func (r *received) IsData() bool {
	return len(r.Data) != 0
}

type header struct {
	code       OPCODE
	version    VER
	final      bool
	payloadLen uint16
}

// To ensure safety all the data past (max uint16 value - 30) bytes will be cut and only first (max uint16 value - 30)
// bytes will be put
// into a payload. Limits might and probably will be increased in future.
func NewPayload(opcode OPCODE, body []byte) Payload {
	return Payload{opcode: opcode, body: body}
}

func constructHeader(dst []byte, code OPCODE, final bool, payloadLen uint16) {
	dst[0] = 0b000<<5 | byte(code)
	var isFinal byte
	if final {
		isFinal = 1
	} else {
		isFinal = 0
	}
	dst[1] = byte(VERSION)<<1 | isFinal<<3 | byte(payloadLen>>8&0b0000_0111)
	dst[2] = byte(payloadLen & 0b1111_1111)
}

// 3 reserved, 5 opcode, 4 ver, 1 final, 11 length
func parseHeader(head []byte) (final bool, version VER, code OPCODE, length uint16, err error) {
	if len(head) != HEAD_SIZE {
		err = errors.New("HEAD SIZE TOO BIG")
		return
	}
	code = OPCODE(head[0] & 0b0001_1111)
	version = VER(head[1] >> 4)
	final = head[1]>>3&0b0000_0001 == 1
	length = uint16(head[1])&0b0000_0111<<8 | uint16(head[2])

	if version != VERSION {
		err = errors.New("VERSION NOT ALLOWED")
		return
	}

	if code > ANSWER || code < REQUEST {
		err = errors.New("OPCODE OUT OF RANGE")
		return
	}

	if length > uint16(MAX_PACKET_SIZE-HEAD_SIZE) {
		err = errors.New("not allowed length")
		return
	}

	if !final && length != uint16(MAX_PACKET_SIZE-HEAD_SIZE) {
		err = errors.New("can not be not final frame and not full length")
		return
	}

	return
}
