/*
This file is part of SecuryChat.
Copyright (C) 2024 Laljaka

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package protocol

import (
	"bytes"
	"crypto/cipher"
	"errors"
	"net"

	"gitlab.com/securychat/securychat-server/encryption"
)

type Connection struct {
	conn *net.TCPConn

	sender        *sender
	senderCounter [12]byte
	senderAEAD    cipher.AEAD

	receiver        *receiver
	receiverCounter [12]byte
	receiverAEAD    cipher.AEAD
}

func UpgradeConnection(conn *net.TCPConn) (*Connection, error) {
	privKey, pubKey, err := encryption.GenerateKeyPair()
	if err != nil {
		return nil, err
	}

	sender := newSender()
	receiver := newReceiver()

	encoded := NewPayload(ECDH, pubKey).encode()

	slice, err := sender.getSlice(len(encoded))
	if err != nil {
		return nil, err
	}
	copy(slice, encoded)

	_, err = conn.Write(sender.getFullSlice())
	if err != nil {
		return nil, err
	}

	rec, err := receiver.receiveFull(conn)
	if err != nil {
		return nil, err
	}

	recv, err := parse(rec)
	if err != nil {
		return nil, err
	}
	if recv.Opcode != ECDH || !recv.IsData {
		return nil, errors.New("wrong")
	}

	recvKey, sendKey, err := encryption.SessionKeys(privKey, recv.Data, pubKey)
	if err != nil {
		return nil, err
	}

	recvAEAD, err := encryption.NewAESGCM(recvKey)
	if err != nil {
		return nil, err
	}

	sendAEAD, err := encryption.NewAESGCM(sendKey)
	if err != nil {
		return nil, err
	}

	return &Connection{
		conn:         conn,
		receiver:     receiver,
		receiverAEAD: recvAEAD,
		sender:       sender,
		senderAEAD:   sendAEAD,
	}, nil
}

func (c *Connection) Write(pl Payload) error {
	encoded := pl.encode()

	slice, err := c.sender.getSlice(len(encoded) + 12 + 16)
	if err != nil {
		return err
	}

	copy(slice, c.senderCounter[:])

	c.senderAEAD.Seal(slice[12:12:len(encoded)+16], c.senderCounter[:], encoded, nil)

	_, err = c.conn.Write(c.sender.getFullSlice()) //TODO split between messages if needed
	if err != nil {
		return err
	}

	addOne(&c.senderCounter)

	return nil
}

func (c *Connection) Receive() (received, error) {
	var empty received

	rec, err := c.receiver.receiveFull(c.conn)
	if err != nil {
		return empty, err
	}

	nonce := rec[:12]
	if !bytes.Equal(nonce, c.receiverCounter[:]) {
		return empty, errors.New("counter is off")
	}

	dec, err := c.receiverAEAD.Open(nil, nonce, rec[12:], nil)
	if err != nil {
		return empty, err
	}

	recv, err := parse(dec)
	if err != nil {
		return empty, err
	}

	addOne(&c.receiverCounter)

	return recv, nil

}

func addOne(ba *[12]byte) {
	for i := 11; i >= 0; i-- {
		ba[i]++
		if ba[i] != 0 {
			break
		}
	}
}

func (c *Connection) Close() {
	c.conn.Close()
}

func (c *Connection) RemoteAddr() net.Addr {
	return c.conn.RemoteAddr()
}
