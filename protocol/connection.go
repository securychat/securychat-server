/*
This file is part of SecuryChat.
Copyright (C) 2024 Laljaka

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package protocol

import (
	"crypto/cipher"
	"errors"
	"net"

	"gitlab.com/securychat/securychat-server/encryption"
)

type Connection struct {
	conn *net.TCPConn

	sender        *sender
	senderCounter [12]byte
	senderAEAD    cipher.AEAD

	receiver        *receiver
	receiverCounter [12]byte
	receiverAEAD    cipher.AEAD
}

func UpgradeConnection(conn *net.TCPConn, isServer bool) (*Connection, error) {
	privKey, pubKey, err := encryption.GenerateKeyPair()
	if err != nil {
		return nil, err
	}

	sender := newSender()
	receiver := newReceiver()

	paylod := NewPayload(ECDH, pubKey)

	head, err := sender.getSlice(HEAD_SIZE)
	if err != nil {
		return nil, err
	}
	constructHeader(head, paylod.opcode, true, uint16(len(paylod.body)))

	body, err := sender.getSlice(len(paylod.body))
	if err != nil {
		return nil, err
	}
	copy(body, paylod.body)

	err = sender.flush(conn)
	if err != nil {
		return nil, err
	}

	header, rec, err := receiver.receivePacket(conn)
	if err != nil {
		return nil, err
	}
	if header.code != ECDH || len(rec) != 32 || !header.final {
		return nil, errors.New("wrong")
	}

	var recvKey, sendKey []byte
	if isServer {
		recvKey, sendKey, err = encryption.SessionKeysAsServer(privKey, pubKey, rec)
	} else {
		recvKey, sendKey, err = encryption.SessionKeysAsClient(privKey, pubKey, rec)
	}
	if err != nil {
		return nil, err
	}

	recvAEAD, err := encryption.NewAESGCM(recvKey)
	if err != nil {
		return nil, err
	}

	sendAEAD, err := encryption.NewAESGCM(sendKey)
	if err != nil {
		return nil, err
	}

	return &Connection{
		conn:         conn,
		receiver:     receiver,
		receiverAEAD: recvAEAD,
		sender:       sender,
		senderAEAD:   sendAEAD,
	}, nil
}

func (c *Connection) Write(pl Payload) error {
	if len(pl.body) == 0 {
		header, err := c.sender.getSlice(HEAD_SIZE)
		if err != nil {
			return err
		}

		constructHeader(header, pl.opcode, true, uint16(AUTH_TAG))

		body, err := c.sender.getSlice(AUTH_TAG)
		if err != nil {
			return err
		}

		c.senderAEAD.Seal(body[:0], c.senderCounter[:], pl.body, header)

		err = c.sender.flush(c.conn)
		if err != nil {
			return err
		}

		addOne(&c.senderCounter)
	} else {
		for i := 0; i < len(pl.body); i += SOFT_PAYLOAD_SIZE {
			upTo := min(i+SOFT_PAYLOAD_SIZE, len(pl.body))
			payloadLen := upTo - i

			header, err := c.sender.getSlice(HEAD_SIZE)
			if err != nil {
				return err
			}

			constructHeader(header, pl.opcode, i+SOFT_PAYLOAD_SIZE >= len(pl.body), uint16(payloadLen+AUTH_TAG))

			body, err := c.sender.getSlice(payloadLen + AUTH_TAG)
			if err != nil {
				return err
			}

			c.senderAEAD.Seal(body[:0], c.senderCounter[:], pl.body[i:upTo], header)

			err = c.sender.flush(c.conn)
			if err != nil {
				return err
			}

			addOne(&c.senderCounter)
		}
	}

	return nil
}

func (c *Connection) Receive() (recv received, err error) {
	var final bool
	var first bool = true

	for !final {
		header, rec, err := c.receiver.receivePacket(c.conn)
		if err != nil {
			return recv, err
		}

		if first {
			recv.Opcode = header.code
			recv.Ver = header.version
			first = false
		}

		dec, err := c.receiverAEAD.Open(nil, c.receiverCounter[:], rec, c.receiver.head[:])
		if err != nil {
			return recv, err
		}

		addOne(&c.receiverCounter)

		if recv.Opcode != header.code || recv.Ver != header.version {
			return recv, errors.New("packets in chain don't match")
		}

		recv.Data = append(recv.Data, dec...)
		final = header.final
	}

	return recv, nil

}

func addOne(ba *[12]byte) {
	for i := 11; i >= 0; i-- {
		ba[i]++
		if ba[i] != 0 {
			break
		}
	}
}

func (c *Connection) Close() {
	c.conn.Close()
}

func (c *Connection) RemoteAddr() net.Addr {
	return c.conn.RemoteAddr()
}
