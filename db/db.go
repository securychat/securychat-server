/*
This file is part of SecuryChat.
Copyright (C) 2024 Laljaka

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package db

import (
	"database/sql"
	"fmt"
)

type ChatID int64
type UserID int64

type Database interface {
	//base
	Prepare() error
	Close() error
	//config
	GetHandle() (uint32, error)
	SetHandle(data uint32) error
	//users
	GetAllUsers() (map[UserID]string, error)
	ValidateOrAddUser(user string, hash string) (uid UserID, valid, added bool, err error)
	GetUserID(user string) (UserID, error)
	DeleteUser(id UserID) error
	//chats
	AddChat(name string) (ChatID, error)
	DeleteChat(id ChatID) error
	GetAllChats() (map[ChatID][]UserID, error)
	//userchats
	LinkUser(uID UserID, cID ChatID) error
	UnlinkUser(uID UserID, cID ChatID) error
	GetUserChats(uID UserID) ([]ChatID, error)
	//utils
	GetRawDB() *sql.DB
}

func CreateSqliteDB(filename string) (*sqlite3DB, error) {
	var db *sql.DB
	var err error

	if filename == "testing" {
		db, err = sql.Open("sqlite3", ":memory:")
	} else {
		db, err = sql.Open("sqlite3", fmt.Sprintf("file:%s.db?_foreign_keys=on", filename))
	}
	if err != nil {
		return nil, newDBCreationError("Error while opening database file", err)
	}

	tx, err := db.Begin()
	if err != nil {
		return nil, newDBCreationError("Can not start transaction", err)
	}
	defer tx.Rollback()

	_, err = tx.Exec(
		`CREATE TABLE IF NOT EXISTS users ( 
  			id INTEGER PRIMARY KEY NOT NULL, 
  			username TEXT NOT NULL UNIQUE, 
  			hash TEXT NOT NULL
		);`,
	)
	if err != nil {
		return nil, newDBCreationError("Can not create users table", err)
	}

	_, err = tx.Exec(
		`CREATE TABLE IF NOT EXISTS chats (
			id INTEGER PRIMARY KEY NOT NULL,
			name TEXT NOT NULL,
			is_group BOOLEAN DEFAULT 0
		);`,
	)
	if err != nil {
		return nil, newDBCreationError("Can not create chats table", err)
	}

	_, err = tx.Exec(
		`CREATE TABLE IF NOT EXISTS user_chats (
			user_id INTEGER NOT NULL,
			chat_id INTEGER NOT NULL,
			PRIMARY KEY (user_id, chat_id),
			FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE,
			FOREIGN KEY (chat_id) REFERENCES chats(id) ON DELETE CASCADE
		);`,
	)
	if err != nil {
		return nil, newDBCreationError("Can not create user_chats table", err)
	}

	_, err = tx.Exec(
		`CREATE TABLE IF NOT EXISTS messages (
    		id INTEGER PRIMARY KEY NOT NULL,
    		chat_id INTEGER NOT NULL,
    		sender_id INTEGER NOT NULL,
    		content BLOB NOT NULL,
    		timestamp DATETIME DEFAULT CURRENT_TIMESTAMP,
    		FOREIGN KEY (chat_id) REFERENCES chats(id) ON DELETE CASCADE,
    		FOREIGN KEY (sender_id) REFERENCES users(id) ON DELETE CASCADE
		);`,
	)
	if err != nil {
		return nil, newDBCreationError("Can not create messages table", err)
	}

	err = tx.Commit()
	if err != nil {
		return nil, newDBCreationError("Can not commit transaction", err)
	}

	return &sqlite3DB{
		db:         db,
		isPrepared: false,
	}, nil
}
