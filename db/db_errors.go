/*
This file is part of SecuryChat.
Copyright (C) 2024 Laljaka

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package db

import "fmt"

type DBCreationError struct {
	reason   string
	original error
}

func (err *DBCreationError) Error() string {
	return fmt.Sprintf("Error while creating DB with reason:\n%s\nwith original error:\n%s", err.reason, err.original.Error())
}

func newDBCreationError(reason string, original error) error {
	return &DBCreationError{reason, original}
}

func newChatFullErr() error {
	return &ChatFullErr{"Chat is full"}
}

type ChatFullErr struct {
	s string
}

func (err *ChatFullErr) Error() string {
	return err.s
}
