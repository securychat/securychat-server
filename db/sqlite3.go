/*
This file is part of SecuryChat.
Copyright (C) 2024 Laljaka

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package db

import (
	"database/sql"
	"errors"
)

type sqlite3DB struct {
	db               *sql.DB
	isPrepared       bool
	getUserIDSTMT    *sql.Stmt
	addChatSTMT      *sql.Stmt
	linkUserSTMT     *sql.Stmt
	unlinkUserSTMT   *sql.Stmt
	getUserChatsSTMT *sql.Stmt
	getAllUsers      *sql.Stmt
}

const (
	//preppable
	addUser      string = "INSERT INTO users(username, hash) VALUES(?, ?)"
	validateUser string = "SELECT COUNT(1) AS match_found FROM users WHERE id = ? AND hash = ?;"
	getUser      string = "SELECT (id) FROM users WHERE username = ?"
	deleteUser   string = "DELETE FROM users WHERE id = ?"
	addChat      string = "INSERT INTO chats(name) VALUES(?)"
	linkUser     string = "INSERT INTO user_chats(user_id, chat_id) SELECT ?, ? WHERE ((SELECT COUNT(*) FROM user_chats WHERE chat_id = ?) < (CASE WHEN (SELECT is_group FROM chats WHERE id = ?) = 0 THEN 2 ELSE 9999999 END));" //"INSERT INTO user_chats(user_id, chat_id) VALUES(?, ?)"
	unlinkUser   string = "DELETE FROM user_chats WHERE user_id = ? AND chat_id = ?"
	getUserChats string = "SELECT chat_id FROM user_chats WHERE user_id = ?"

	//unpreppable
	checkConfigured  string = "PRAGMA user_version;"
	checkConfigured2 string = "SELECT COUNT(1) AS match_found FROM sqlite_master WHERE type = 'table' AND name = ?;"
	setConfigured    string = "PRAGMA user_version = ?;"
	loadAllParams    string = "SELECT key, value FROM config"
	loadParam        string = "SELECT value FROM config WHERE key = ?"
	storeParam       string = "INSERT INTO config(key, value) VALUES(?, ?)"
	updateParam      string = "UPDATE config SET value = ? WHERE key = ?;"
	removeParam      string = "DELETE FROM config WHERE key = ?"
	getAllUsers      string = "SELECT id, username FROM users"
	getAllChats      string = "SELECT chats.id, user_chats.user_id FROM chats LEFT JOIN user_chats ON chats.id = user_chats.chat_id"
	deleteChat       string = "DELETE FROM chats WHERE id = ?"
)

//base ------------------------------------------------------------------------------

func (DB *sqlite3DB) Prepare() (err error) {
	prep := func(st string) *sql.Stmt {
		if err != nil {
			return nil
		}

		var res *sql.Stmt
		res, err = DB.db.Prepare(st)
		return res
	}
	DB.getUserIDSTMT = prep(getUser)
	DB.addChatSTMT = prep(addChat)
	DB.linkUserSTMT = prep(linkUser)
	DB.unlinkUserSTMT = prep(unlinkUser)
	DB.getUserChatsSTMT = prep(getUserChats)
	DB.getAllUsers = prep(getAllUsers)

	if err != nil {
		return err
	}

	DB.isPrepared = true
	return nil
}

func (DB *sqlite3DB) Close() (err error) {
	if DB.isPrepared {
		close := func(st *sql.Stmt) {
			if err != nil {
				return
			}
			err = st.Close()
		}
		close(DB.getUserIDSTMT)
		close(DB.addChatSTMT)
		close(DB.linkUserSTMT)
		close(DB.unlinkUserSTMT)
		close(DB.getUserChatsSTMT)
		close(DB.getAllUsers)
		if err != nil {
			return
		}
	}
	return DB.db.Close()
}

func (DB *sqlite3DB) GetHandle() (uint32, error) {
	var version uint32
	err := DB.db.QueryRow(checkConfigured).Scan(&version)
	if err != nil {
		return 0, err
	}

	return version, nil
}

func (DB *sqlite3DB) SetHandle(data uint32) error {
	_, err := DB.db.Exec(setConfigured, data)
	if err != nil {
		return err
	}

	return nil
}

//users -------------------------------------------------------------------------------

func (DB *sqlite3DB) GetAllUsers() (map[UserID]string, error) {
	rows, err := DB.db.Query(getAllUsers)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	usrs := make(map[UserID]string)
	for rows.Next() {
		var id int64
		var name string

		err = rows.Scan(&id, &name)
		if err != nil {
			return nil, err
		}

		usrs[UserID(id)] = name
	}

	return usrs, nil
}

func (DB *sqlite3DB) DeleteUser(id UserID) error {
	res, err := DB.db.Exec(deleteUser, int64(id))
	if err != nil {
		return err
	}

	r, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if r < 1 {
		return errors.New("deleting did nothing")
	}

	return nil
}

func (DB *sqlite3DB) ValidateOrAddUser(user string, hash string) (uid UserID, valid, added bool, err error) {
	tx, err := DB.db.Begin()
	if err != nil {
		return 0, false, false, err
	}
	defer tx.Rollback()

	var id int
	err = tx.QueryRow(getUser, user).Scan(&id)
	if err == sql.ErrNoRows {
		res, err := tx.Exec(addUser, user, hash)
		if err != nil {
			return 0, false, false, err
		}

		newid, err := res.LastInsertId()
		if err != nil {
			return 0, false, false, err
		}

		err = tx.Commit()
		if err != nil {
			return 0, false, false, err
		}

		return UserID(newid), false, true, nil
	} else {
		if err != nil {
			return 0, false, false, err
		}
	}

	var found int
	err = tx.QueryRow(validateUser, id, hash).Scan(&found)
	if err != nil {
		return 0, false, false, err
	}

	err = tx.Commit()
	if err != nil {
		return 0, false, false, err
	}

	return UserID(id), found == 1, false, nil
}

func (DB *sqlite3DB) GetUserID(name string) (UserID, error) {
	var id int64

	var err error
	if DB.isPrepared {
		err = DB.getUserIDSTMT.QueryRow(name).Scan(&id)
	} else {
		err = DB.db.QueryRow(getUser, name).Scan(&id)
	}
	if err != nil {
		if err != sql.ErrNoRows {
			return -1, err
		}
		return 0, nil
	}

	return UserID(id), nil
}

//chats -------------------------------------------------------------------------------

func (DB *sqlite3DB) AddChat(name string) (ChatID, error) {
	var res sql.Result
	var err error

	if DB.isPrepared {
		res, err = DB.addChatSTMT.Exec(name)
	} else {
		res, err = DB.db.Exec(addChat, name)
	}
	if err != nil {
		return -1, err
	}

	id, err := res.LastInsertId()
	if err != nil {
		return -1, err
	}

	return ChatID(id), nil
}

func (DB *sqlite3DB) GetAllChats() (map[ChatID][]UserID, error) {

	chats := make(map[ChatID][]UserID) // TODO 0 len everywhere

	rows, err := DB.db.Query(getAllChats)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var cid int64
	var uid int64
	for rows.Next() {
		err = rows.Scan(&cid, &uid)
		if err != nil {
			return nil, err
		}

		chats[ChatID(cid)] = append(chats[ChatID(cid)], UserID(uid))
	}

	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return chats, nil
}

func (DB *sqlite3DB) DeleteChat(id ChatID) error {
	res, err := DB.db.Exec(deleteChat, int64(id))
	if err != nil {
		return err
	}

	r, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if r < 1 {
		return errors.New("deleting did nothing")
	}

	return nil
}

//userchats ---------------------------------------------------------------------------

func (DB *sqlite3DB) LinkUser(uID UserID, cID ChatID) error {
	var res sql.Result
	var err error

	if DB.isPrepared {
		res, err = DB.linkUserSTMT.Exec(int64(uID), int64(cID), int64(cID), int64(cID))
	} else {
		res, err = DB.db.Exec(linkUser, int64(uID), int64(cID), int64(cID), int64(cID))
	}
	if err != nil {
		return err
	}

	affected, err := res.RowsAffected()
	if err != nil {
		return err
	}

	if affected == 0 {
		return newChatFullErr()
	}

	return nil
}

func (DB *sqlite3DB) UnlinkUser(uID UserID, cID ChatID) error {
	var err error

	if DB.isPrepared {
		_, err = DB.unlinkUserSTMT.Exec(int64(uID), int64(cID))
	} else {
		_, err = DB.db.Exec(unlinkUser, int64(uID), int64(cID))
	}
	if err != nil {
		return err
	}

	return nil
}

func (DB *sqlite3DB) GetUserChats(uID UserID) ([]ChatID, error) {
	var rows *sql.Rows
	var err error

	if DB.isPrepared {
		rows, err = DB.getUserChatsSTMT.Query(int64(uID))
	} else {
		rows, err = DB.db.Query(getUserChats, int64(uID))
	}
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	ints := make([]ChatID, 0)
	for rows.Next() {
		var i int64

		err = rows.Scan(&i)
		if err != nil {
			return nil, err
		}

		ints = append(ints, ChatID(i))
	}

	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return ints, nil
}

// utils ------------------------------------------------------------------------------

func (DB *sqlite3DB) GetRawDB() *sql.DB {
	return DB.db
}
