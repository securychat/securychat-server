/*
This file is part of SecuryChat.
Copyright (C) 2024 Laljaka

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/
package db

import (
	"reflect"
	"testing"

	_ "github.com/mattn/go-sqlite3"
)

func TestCreateSqliteDB(t *testing.T) {
	db, err := CreateSqliteDB("testing")
	if err != nil {
		t.Fatal(err)
	}

	err = db.Prepare()
	if err != nil {
		t.Fatal(err)
	}

	err = db.Close()
	if err != nil {
		t.Fatal(err)
	}
}

func TestGetSetHandle(t *testing.T) {
	db, err := CreateSqliteDB("testing")
	if err != nil {
		t.Fatal(err)
	}

	v, err := db.GetHandle()
	if err != nil {
		t.Fatal(err)
	}
	if v != 0 {
		t.Fatal("expected to be 0")
	}

	var expect uint32 = 0xFFFFFFFF
	err = db.SetHandle(expect)
	if err != nil {
		t.Fatal(err)
	}

	g, err := db.GetHandle()
	if err != nil {
		t.Fatal(err)
	}
	if g != expect {
		t.Fatal("expected to get the same number")
	}

	err = db.Close()
	if err != nil {
		t.Fatal(err)
	}
}

func TestValidateOrAddUser(t *testing.T) {
	db, err := CreateSqliteDB("testing")
	if err != nil {
		t.Fatal(err)
	}
	tests := []struct {
		name          string
		user          string
		hash          string
		expectedValid bool
		expectedAdded bool
		expectedErr   bool
		preInsertUser string
		preInsertHash string
	}{
		{
			name:          "New user is added",
			user:          "newuser",
			hash:          "newhash",
			expectedValid: false,
			expectedAdded: true,
			expectedErr:   false,
		},
		{
			name:          "Existing user is validated",
			user:          "existinguser",
			hash:          "existinghash",
			expectedValid: true,
			expectedAdded: false,
			expectedErr:   false,
			preInsertUser: "existinguser",
			preInsertHash: "existinghash",
		},
		{
			name:          "Existing user with wrong hash is not valid",
			user:          "existinguser",
			hash:          "wronghash",
			expectedValid: false,
			expectedAdded: false,
			expectedErr:   false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.preInsertUser != "" {
				_, err := db.GetRawDB().Exec(addUser, tt.preInsertUser, tt.preInsertHash)
				if err != nil {
					t.Fatalf("Failed to insert pre-existing user: %v", err)
				}
			}

			uid, valid, added, err := db.ValidateOrAddUser(tt.user, tt.hash)

			if tt.expectedErr {
				if err == nil {
					t.Fatalf("Expected an error but got none")
				}
				return
			}

			if err != nil {
				t.Fatalf("Unexpected error: %v", err)
			}

			if valid != tt.expectedValid {
				t.Errorf("Expected valid: %v, got: %v", tt.expectedValid, valid)
			}
			if added != tt.expectedAdded {
				t.Errorf("Expected added: %v, got: %v", tt.expectedAdded, added)
			}

			if added && uid == 0 {
				t.Errorf("Expected a non-zero UserID for a newly added user, got: %d", uid)
			}
		})
	}

	err = db.Close()
	if err != nil {
		t.Fatal(err)
	}
}

func TestGetUserID(t *testing.T) {
	db, err := CreateSqliteDB("testing")
	if err != nil {
		t.Fatal(err)
	}

	_, err = db.GetRawDB().Exec(addUser, "TestUser1", "test")
	if err != nil {
		t.Fatal(err)
	}

	id, err := db.GetUserID("TestUser1")
	if err != nil {
		t.Fatal(err)
	}
	if id != 1 {
		t.Fatal("ID expected to be 1")
	}

	id, err = db.GetUserID("WrongUser1")
	if err != nil {
		t.Fatal(err)
	}
	if id != 0 {
		t.Fatal("ID expected to be 0")
	}

	err = db.Close()
	if err != nil {
		t.Fatal(err)
	}
}

func TestGetAllUsers(t *testing.T) {
	expected := make(map[UserID]string)
	expected[1] = "TestUser1"
	expected[2] = "TestUser2"

	db, err := CreateSqliteDB("testing")
	if err != nil {
		t.Fatal(err)
	}

	_, err = db.GetRawDB().Exec(addUser, "TestUser1", "test")
	if err != nil {
		t.Fatal(err)
	}

	_, err = db.GetRawDB().Exec(addUser, "TestUser2", "test")
	if err != nil {
		t.Fatal(err)
	}

	users, err := db.GetAllUsers()
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(users, expected) {
		t.Fatal("Expected different users")
	}

	err = db.Close()
	if err != nil {
		t.Fatal(err)
	}
}

func TestDeleteUser(t *testing.T) {
	db, err := CreateSqliteDB("testing")
	if err != nil {
		t.Fatal(err)
	}

	err = db.DeleteUser(1)
	if err == nil {
		t.Fatal("expected to error")
	}

	res, err := db.GetRawDB().Exec(addUser, "test", "test")
	if err != nil {
		t.Fatal(err)
	}

	user, err := res.LastInsertId()
	if err != nil {
		t.Fatal(err)
	}

	err = db.DeleteUser(UserID(user))
	if err != nil {
		t.Fatal(err)
	}

	err = db.Close()
	if err != nil {
		t.Fatal(err)
	}
}

func TestAddChatDeleteChat(t *testing.T) {
	db, err := CreateSqliteDB("testing")
	if err != nil {
		t.Fatal(err)
	}

	err = db.DeleteChat(1)
	if err == nil {
		t.Fatal("Expected to error")
	}

	id, err := db.AddChat("TestChat1")
	if err != nil {
		t.Fatal(err)
	}
	if id != 1 {
		t.Fatal("ID is expected to be 1")
	}

	id, err = db.AddChat("TestChat2")
	if err != nil {
		t.Fatal(err)
	}
	if id != 2 {
		t.Fatal("ID is expected to be 2")
	}

	err = db.DeleteChat(id)
	if err != nil {
		t.Fatal(err)
	}

	err = db.Close()
	if err != nil {
		t.Fatal(err)
	}
}

func TestLinkUser(t *testing.T) {
	db, err := CreateSqliteDB("testing")
	if err != nil {
		t.Fatal(err)
	}

	chat, err := db.AddChat("test")
	if err != nil {
		t.Fatal(err)
	}

	res, err := db.GetRawDB().Exec(addUser, "test", "test")
	if err != nil {
		t.Fatal(err)
	}
	user, err := res.LastInsertId()
	if err != nil {
		t.Fatal(err)
	}

	err = db.LinkUser(UserID(user), chat)
	if err != nil {
		t.Fatal(err)
	}

	err = db.Close()
	if err != nil {
		t.Fatal(err)
	}
}

/*
func TestGetUserChats(t *testing.T) {
	expected := make([]ChatID, 2)
	expected[0] = 1
	expected[1] = 2

	chats, err := db.GetUserChats(2)
	if err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(chats, expected) {
		t.Fatal("Expected different chats")
	}
}

func TestGetAllChats(t *testing.T) {
	expected := make(map[ChatID][]UserID)
	expected[2] = make([]UserID, 1)
	expected[2][0] = 2
	expected[1] = make([]UserID, 2)
	expected[1][0] = 1
	expected[1][1] = 2
	m, err := db.GetAllChats()
	if err != nil {
		t.Fatal(err)
	}

	if !reflect.DeepEqual(m, expected) {
		t.Fatal("Expected different map")
	}

}

*/
