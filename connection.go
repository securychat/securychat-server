/*
This file is part of SecuryChat.
Copyright (C) 2024 Laljaka

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"context"
	"errors"
	"fmt"
	"io"
	"runtime"
	"strconv"
	"sync"

	"gitlab.com/securychat/securychat-server/db"
	"gitlab.com/securychat/securychat-server/protocol"
)

type Connection2 struct {
	*protocol.Connection
	owner db.UserID
	oRef  *MessageUser
	ch    chan protocol.Payload
	sbus  chan<- ServerMSG
}

func (c *Connection2) handleSend(pctx context.Context, wg *sync.WaitGroup, closech <-chan interface{}) {
	defer wg.Done()
	defer close(c.ch)
	defer drainChannelWithoutClose(c.ch)
	defer fmt.Println(c.RemoteAddr(), "died")
	defer c.oRef.Offline()

	for msg := range c.oRef.GetMsgs() {
		err := c.Write(msg)
		if err != nil {
			fmt.Println(err)
			c.Close()
			break
		}
	}
	for {
		select {
		case msg := <-c.ch:
			err := c.Write(msg)
			if err != nil {
				fmt.Println(err)
				c.Close()
			}
		case <-closech:
			return
		case <-pctx.Done():
			c.Close()
		}
		runtime.Gosched()
	}
}

func (c *Connection2) handleRecv(closech chan<- interface{}) {
	defer close(closech)
	for {
		recv, err := c.Receive()
		if err != nil {
			if errors.Is(err, io.EOF) {
				fmt.Println("Connection closed from the other side")
			} else {
				fmt.Println(err)
			}
			return
		}

		switch recv.Opcode {
		case protocol.CLOSE:
			fmt.Println("client asking to close the connection, proper closure")
			return
		case protocol.MESSAGE:
			c.oRef.SendTo(db.ChatID(recv.Data[0]), protocol.NewPayload(protocol.MESSAGE, recv.Data)) // TODO DO NOT TRUST USERS' CHAT ID (OR ANYTHING)
		case protocol.TUNNEL:
			c.sbus <- ServerMSG{c.owner, MAKE_PRIVATE_CHAT, string(recv.Data)}
		case protocol.ANSWER:
			if recv.Data[1] == 1 {
				c.sbus <- ServerMSG{c.owner, ADD_ME, strconv.Itoa(int(recv.Data[0]))}
				fmt.Println("addme?")
			} else {
				c.sbus <- ServerMSG{c.owner, NOT_ADD_ME, strconv.Itoa(int(recv.Data[0]))}
			}
		default:
			fmt.Println("OPCODE NOT ALLOWED")
			return
		}
		runtime.Gosched()
	}
}
