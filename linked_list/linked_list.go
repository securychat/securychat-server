/*
This file is part of SecuryChat.
Copyright (C) 2024 Laljaka

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package linked_list

import "sync"

type node[T any] struct {
	value T
	next  *node[T]
}

type LinkedList[T any] struct {
	head *node[T]
	tail *node[T]
	mu   sync.Mutex
	//size int
}

func (l *LinkedList[T]) Append(v T) {
	l.mu.Lock()
	defer l.mu.Unlock()
	newNode := &node[T]{value: v}

	if l.head == nil {
		l.head = newNode
		l.tail = newNode
	} else {
		l.tail.next = newNode
		l.tail = newNode
	}

	//l.size++
}

func (l *LinkedList[T]) GrabFirst() (T, bool) {
	l.mu.Lock()
	defer l.mu.Unlock()
	if l.head == nil {
		var z T
		return z, false
	}

	v := l.head.value
	l.head = l.head.next

	if l.head == nil {
		l.tail = nil
	}

	return v, true
}
