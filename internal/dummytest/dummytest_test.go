/*
This file is part of SecuryChat.
Copyright (C) 2024 Laljaka

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package dummytest

import (
	"encoding/binary"
	"testing"
)

// TwoStrings holds two string values.
type TwoStrings struct {
	s1 string
	s2 string
}

// useStruct accesses the struct fields.
func useStruct(ts TwoStrings, ba *[655]byte) {
	binary.BigEndian.PutUint16(ba[:], uint16(len(ts.s1)))
	copy(ba[2:], ts.s1)
	copy(ba[2+len(ts.s1):], ts.s2)
}

// useStructPtr accesses the struct fields via pointer.
func useStructPtr(ts *TwoStrings, ba *[655]byte) {
	binary.BigEndian.PutUint16(ba[:], uint16(len(ts.s1)))
	copy(ba[2:], ts.s1)
	copy(ba[2+len(ts.s1):], ts.s2)
}

// BenchmarkStructValue creates a TwoStrings struct by value, passes it to useStruct.
func BenchmarkStructValue(b *testing.B) {
	s1 := "hello"
	s2 := "world"
	var r [655]byte
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		ts := TwoStrings{s1: s1, s2: s2}
		useStruct(ts, &r)
	}
	_ = r
}

// BenchmarkStructPointer creates a TwoStrings struct and passes a pointer to useStructPtr.
func BenchmarkStructPointer(b *testing.B) {
	s1 := "hello"
	s2 := "world"
	var r [655]byte
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		ts := &TwoStrings{s1: s1, s2: s2}
		useStructPtr(ts, &r)
	}
	_ = r
}

// encodeSlice creates a byte slice that starts with a 2-byte big endian
// representation of the length of s1 (using binary.BigEndian.PutUint16),
// followed by s1 and then s2.
func encodeSlice(s1, s2 string) []byte {
	bs := make([]byte, 2+len(s1)+len(s2))
	// Encode the length of s1 as a big endian uint16.
	binary.BigEndian.PutUint16(bs[:2], uint16(len(s1)))
	copy(bs[2:], s1)
	copy(bs[2+len(s1):], s2)
	return bs
}

// useSlice parses the slice and accesses the two strings.
func useSlice(bs []byte, ba *[655]byte) {
	copy(ba[:], bs)
}

// BenchmarkSlice creates the encoded slice once and then repeatedly parses it.
func BenchmarkSlice(b *testing.B) {
	s1 := "hello"
	s2 := "world"
	var r [655]byte
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		bs := encodeSlice(s1, s2)
		useSlice(bs, &r)
	}
	_ = r
}
