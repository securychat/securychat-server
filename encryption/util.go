/*
This file is part of SecuryChat.
Copyright (C) 2024 Laljaka

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package encryption

import (
	"reflect"
	"unsafe"

	"github.com/google/go-tpm/tpm2"
)

func TPMAlgIDString(alg tpm2.TPMAlgID) string {
	switch alg {
	case tpm2.TPMAlgRSA:
		return "RSA"
	case tpm2.TPMAlgTDES:
		return "TDES"
	case tpm2.TPMAlgSHA1:
		return "SHA-1"
	case tpm2.TPMAlgHMAC:
		return "HMAC"
	case tpm2.TPMAlgAES:
		return "AES"
	case tpm2.TPMAlgMGF1:
		return "MGF1"
	case tpm2.TPMAlgKeyedHash:
		return "Keyed Hash"
	case tpm2.TPMAlgXOR:
		return "XOR"
	case tpm2.TPMAlgSHA256:
		return "SHA-256"
	case tpm2.TPMAlgSHA384:
		return "SHA-384"
	case tpm2.TPMAlgSHA512:
		return "SHA-512"
	case tpm2.TPMAlgSHA256192:
		return "SHA-256/192"
	case tpm2.TPMAlgNull:
		return "Null"
	case tpm2.TPMAlgSM3256:
		return "SM3-256"
	case tpm2.TPMAlgSM4:
		return "SM4"
	case tpm2.TPMAlgRSASSA:
		return "RSASSA"
	case tpm2.TPMAlgRSAES:
		return "RSAES"
	case tpm2.TPMAlgRSAPSS:
		return "RSAPSS"
	case tpm2.TPMAlgOAEP:
		return "OAEP"
	case tpm2.TPMAlgECDSA:
		return "ECDSA"
	case tpm2.TPMAlgECDH:
		return "ECDH"
	case tpm2.TPMAlgECDAA:
		return "ECDAA"
	case tpm2.TPMAlgSM2:
		return "SM2"
	case tpm2.TPMAlgECSchnorr:
		return "EC-Schnorr"
	case tpm2.TPMAlgECMQV:
		return "ECMQV"
	case tpm2.TPMAlgKDF1SP80056A:
		return "KDF1-SP800-56A"
	case tpm2.TPMAlgKDF2:
		return "KDF2"
	case tpm2.TPMAlgKDF1SP800108:
		return "KDF1-SP800-108"
	case tpm2.TPMAlgECC:
		return "ECC"
	case tpm2.TPMAlgSymCipher:
		return "Symmetric Cipher"
	case tpm2.TPMAlgCamellia:
		return "Camellia"
	case tpm2.TPMAlgSHA3256:
		return "SHA3-256"
	case tpm2.TPMAlgSHA3384:
		return "SHA3-384"
	case tpm2.TPMAlgSHA3512:
		return "SHA3-512"
	case tpm2.TPMAlgSHAKE128:
		return "SHAKE-128"
	case tpm2.TPMAlgSHAKE256:
		return "SHAKE-256"
	case tpm2.TPMAlgSHAKE256192:
		return "SHAKE-256/192"
	case tpm2.TPMAlgSHAKE256256:
		return "SHAKE-256/256"
	case tpm2.TPMAlgSHAKE256512:
		return "SHAKE-256/512"
	case tpm2.TPMAlgCMAC:
		return "CMAC"
	case tpm2.TPMAlgCTR:
		return "CTR"
	case tpm2.TPMAlgOFB:
		return "OFB"
	case tpm2.TPMAlgCBC:
		return "CBC"
	case tpm2.TPMAlgCFB:
		return "CFB"
	case tpm2.TPMAlgECB:
		return "ECB"
	case tpm2.TPMAlgCCM:
		return "CCM"
	case tpm2.TPMAlgGCM:
		return "GCM"
	case tpm2.TPMAlgKW:
		return "KW"
	case tpm2.TPMAlgKWP:
		return "KWP"
	case tpm2.TPMAlgEAX:
		return "EAX"
	case tpm2.TPMAlgEDDSA:
		return "EdDSA"
	case tpm2.TPMAlgEDDSAPH:
		return "EdDSA-PH"
	case tpm2.TPMAlgLMS:
		return "LMS"
	case tpm2.TPMAlgXMSS:
		return "XMSS"
	case tpm2.TPMAlgKEYEDXOF:
		return "Keyed XOF"
	case tpm2.TPMAlgKMACXOF128:
		return "KMAC XOF-128"
	case tpm2.TPMAlgKMACXOF256:
		return "KMAC XOF-256"
	case tpm2.TPMAlgKMAC128:
		return "KMAC-128"
	case tpm2.TPMAlgKMAC256:
		return "KMAC-256"
	default:
		return "Unknown Algorithm"
	}
}

func zeroSlice(slice []byte) {
	for i := range slice {
		slice[i] = 0
	}
}

func zeroMemory(ptr interface{}) {
	v := reflect.ValueOf(ptr).Elem()
	mem := unsafe.Pointer(v.UnsafeAddr())
	size := v.Type().Size()

	zeroed := make([]byte, size)
	copy((*[1 << 30]byte)(mem)[:size:size], zeroed)
}
