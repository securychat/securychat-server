/*
This file is part of SecuryChat.
Copyright (C) 2024 Laljaka

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package encryption

import (
	"bytes"
	"crypto/rand"
	"encoding/hex"
	"io"
	"testing"

	"golang.org/x/crypto/curve25519"
)

func TestZeroSlice(t *testing.T) {
	t.Parallel()
	var expected = new([32]byte)
	var ba = new([32]byte)

	_, err := rand.Read(ba[:])
	if err != nil {
		t.Fatal(err)
	}

	if bytes.Equal(ba[:], expected[:]) {
		t.Fatal("Arrays should not be equal at this point")
	}

	zeroSlice(ba[:])

	if !bytes.Equal(ba[:], expected[:]) {
		t.Fatal("Arrays should be equal at this point")
	}
}

type errorReader struct{}

func (e errorReader) Read(p []byte) (n int, err error) {
	return 0, io.ErrUnexpectedEOF
}

func isZeroed(data []byte) bool {
	for _, b := range data {
		if b != 0 {
			return false
		}
	}
	return true
}

func TestGenerateKeyPair(t *testing.T) {
	t.Parallel()
	t.Run("GenerateKeyPair generates valid keys", func(t *testing.T) {
		privateKey, publicKey, err := GenerateKeyPair()
		if err != nil {
			t.Fatalf("GenerateKeyPair returned an error: %v", err)
		}

		if privateKey == nil {
			t.Fatal("privateKey is nil")
		}
		if publicKey == nil {
			t.Fatal("publicKey is nil")
		}

		if len(privateKey) != 32 {
			t.Fatalf("privateKey length is %d, expected %d", len(privateKey), 32)
		}
		if len(publicKey) != 32 {
			t.Fatalf("publicKey length is %d, expected %d", len(publicKey), 32)
		}

		regeneratedPub, err := curve25519.X25519(privateKey[:], curve25519.Basepoint)
		if err != nil {
			t.Fatalf("Failed to regenerate publicKey from privateKey: %v", err)
		}
		if !bytes.Equal(publicKey[:], regeneratedPub) {
			t.Fatal("publicKey does not match regenerated key from privateKey")
		}
	})

	t.Run("GenerateKeyPair handles random generation failure", func(t *testing.T) {
		oldReader := rand.Reader
		rand.Reader = errorReader{}
		defer func() { rand.Reader = oldReader }()

		privateKey, publicKey, err := GenerateKeyPair()

		if err == nil {
			t.Fatal("Expected error, but got none")
		}

		if privateKey != nil || !isZeroed(privateKey[:]) {
			t.Fatal("privateKey is not zeroed or nil after failure")
		}
		if publicKey != nil || !isZeroed(publicKey[:]) {
			t.Fatal("publicKey should be nil after failure")
		}
	})
}

func TestDeriveSharedSecret(t *testing.T) {
	t.Parallel()
	t.Run("Derives shared secret successfully", func(t *testing.T) {
		serverSK, serverPK, err := GenerateKeyPair()
		if err != nil {
			t.Fatalf("Failed to generate server key pair: %v", err)
		}
		clientSK, clientPK, err := GenerateKeyPair()
		if err != nil {
			t.Fatalf("Failed to generate client key pair: %v", err)
		}

		serverSharedSecret, err := deriveSharedSecret(serverSK, clientPK)
		if err != nil {
			t.Fatalf("Server failed to derive shared secret: %v", err)
		}
		clientSharedSecret, err := deriveSharedSecret(clientSK, serverPK)
		if err != nil {
			t.Fatalf("Client failed to derive shared secret: %v", err)
		}

		if !bytes.Equal(serverSharedSecret[:], clientSharedSecret[:]) {
			t.Fatal("Shared secrets do not match")
		}
	})

	t.Run("Fails with mismatched public keys", func(t *testing.T) {
		serverSK, serverPK, err := GenerateKeyPair()
		if err != nil {
			t.Fatalf("Failed to generate server key pair: %v", err)
		}
		clientSK, _, err := GenerateKeyPair()
		if err != nil {
			t.Fatalf("Failed to generate client key pair: %v", err)
		}

		_, wrongPK, err := GenerateKeyPair()
		if err != nil {
			t.Fatalf("Failed to generate wrong public key: %v", err)
		}

		serverSharedSecret, err := deriveSharedSecret(serverSK, wrongPK)
		if err != nil {
			t.Fatalf("Server failed to derive shared secret with wrong public key: %v", err)
		}
		clientSharedSecret, err := deriveSharedSecret(clientSK, serverPK)
		if err != nil {
			t.Fatalf("Client failed to derive shared secret: %v", err)
		}

		if bytes.Equal(serverSharedSecret[:], clientSharedSecret[:]) {
			t.Fatal("Shared secrets should not match when using an incorrect public key")
		}
	})
}

func TestDeriveSessionKeys(t *testing.T) {
	t.Parallel()
	t.Run("Derives session keys successfully", func(t *testing.T) {
		serverSK, serverPK, err := GenerateKeyPair()
		if err != nil {
			t.Fatalf("Failed to generate server key pair: %v", err)
		}
		_, clientPK, err := GenerateKeyPair()
		if err != nil {
			t.Fatalf("Failed to generate client key pair: %v", err)
		}

		sharedSecret, err := deriveSharedSecret(serverSK, clientPK)
		if err != nil {
			t.Fatalf("Failed to derive shared secret: %v", err)
		}

		rx, tx, err := deriveSessionKeys(sharedSecret, clientPK, serverPK)
		if err != nil {
			t.Fatalf("Failed to derive session keys: %v", err)
		}

		if isZeroed(rx[:]) || isZeroed(tx[:]) {
			t.Fatal("Expected non-nil session keys")
		}
		if bytes.Equal(rx[:], tx[:]) {
			t.Fatal("rx and tx keys should not be equal")
		}
	})

	t.Run("Consistent session keys for identical inputs", func(t *testing.T) {
		serverSK, serverPK, err := GenerateKeyPair()
		if err != nil {
			t.Fatalf("Failed to generate server key pair: %v", err)
		}
		_, clientPK, err := GenerateKeyPair()
		if err != nil {
			t.Fatalf("Failed to generate client key pair: %v", err)
		}

		sharedSecret, err := deriveSharedSecret(serverSK, clientPK)
		if err != nil {
			t.Fatalf("Failed to derive shared secret: %v", err)
		}

		rx1, tx1, err := deriveSessionKeys(sharedSecret, clientPK, serverPK)
		if err != nil {
			t.Fatalf("Failed to derive session keys: %v", err)
		}

		rx2, tx2, err := deriveSessionKeys(sharedSecret, clientPK, serverPK)
		if err != nil {
			t.Fatalf("Failed to derive session keys: %v", err)
		}

		if !bytes.Equal(rx1[:], rx2[:]) || !bytes.Equal(tx1[:], tx2[:]) {
			t.Fatal("Derived session keys are not consistent for identical inputs")
		}
	})

	t.Run("Different inputs yield different session keys", func(t *testing.T) {
		t.Parallel()
		serverSK1, serverPK1, err := GenerateKeyPair()
		if err != nil {
			t.Fatalf("Failed to generate server key pair 1: %v", err)
		}
		_, clientPK1, err := GenerateKeyPair()
		if err != nil {
			t.Fatalf("Failed to generate client key pair 1: %v", err)
		}

		serverSK2, serverPK2, err := GenerateKeyPair()
		if err != nil {
			t.Fatalf("Failed to generate server key pair 2: %v", err)
		}
		_, clientPK2, err := GenerateKeyPair()
		if err != nil {
			t.Fatalf("Failed to generate client key pair 2: %v", err)
		}

		sharedSecret1, err := deriveSharedSecret(serverSK1, clientPK1)
		if err != nil {
			t.Fatalf("Failed to derive shared secret 1: %v", err)
		}

		sharedSecret2, err := deriveSharedSecret(serverSK2, clientPK2)
		if err != nil {
			t.Fatalf("Failed to derive shared secret 2: %v", err)
		}

		rx1, tx1, err := deriveSessionKeys(sharedSecret1, clientPK1, serverPK1)
		if err != nil {
			t.Fatalf("Failed to derive session keys for input 1: %v", err)
		}

		rx2, tx2, err := deriveSessionKeys(sharedSecret2, clientPK2, serverPK2)
		if err != nil {
			t.Fatalf("Failed to derive session keys for input 2: %v", err)
		}

		if bytes.Equal(rx1[:], rx2[:]) || bytes.Equal(tx1[:], tx2[:]) {
			t.Fatal("Derived session keys should differ for different inputs")
		}
	})

	t.Run("Client and server generate matching session keys", func(t *testing.T) {
		serverSK, serverPK, err := GenerateKeyPair()
		if err != nil {
			t.Fatalf("Failed to generate server key pair: %v", err)
		}

		clientSK, clientPK, err := GenerateKeyPair()
		if err != nil {
			t.Fatalf("Failed to generate client key pair: %v", err)
		}

		serverSharedSecret, err := deriveSharedSecret(serverSK, clientPK)
		if err != nil {
			t.Fatalf("Server failed to derive shared secret: %v", err)
		}

		clientSharedSecret, err := deriveSharedSecret(clientSK, serverPK)
		if err != nil {
			t.Fatalf("Client failed to derive shared secret: %v", err)
		}

		serverRx, serverTx, err := deriveSessionKeys(serverSharedSecret, clientPK, serverPK)
		if err != nil {
			t.Fatalf("Server failed to derive session keys: %v", err)
		}

		clientRx, clientTx, err := deriveSessionKeys(clientSharedSecret, clientPK, serverPK)
		if err != nil {
			t.Fatalf("Client failed to derive session keys: %v", err)
		}

		if !bytes.Equal(serverRx[:], clientRx[:]) {
			t.Fatalf("Server rx and Client rx do not match")
		}
		if !bytes.Equal(serverTx[:], clientTx[:]) {
			t.Fatalf("Server tx and Client tx do not match")
		}
	})
}

func TestSessionKeysDefer(t *testing.T) {
	t.Parallel()
	privateKey, publicKey, err := GenerateKeyPair()
	if err != nil {
		t.Fatalf("Failed to generate private/public key pair: %v", err)
	}
	_, otherPublicKey, err := GenerateKeyPair()
	if err != nil {
		t.Fatalf("Failed to generate other private/public key pair: %v", err)
	}

	_, _, err = SessionKeysAsServer(privateKey, publicKey, otherPublicKey)
	if err != nil {
		t.Fatalf("Failed to derive session keys: %v", err)
	}

	if !isZeroed(privateKey[:]) {
		t.Fatal("privateKey was not zeroed after function execution")
	}
}

func TestTest(t *testing.T) {
	skey1, pkey1, err := GenerateKeyPair()
	if err != nil {
		t.Fatal(err)
	}

	skey2, pkey2, err := GenerateKeyPair()
	if err != nil {
		t.Fatal(err)
	}

	key11, key12, err := SessionKeysAsServer(skey1, pkey1, pkey2)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(len(key11))
	t.Log(hex.EncodeToString(key11))
	t.Log(len(key12))
	t.Log(hex.EncodeToString(key12))

	key21, key22, err := SessionKeysAsClient(skey2, pkey2, pkey1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(len(key21))
	t.Log(hex.EncodeToString(key21))
	t.Log(len(key22))
	t.Log(hex.EncodeToString(key22))
}
