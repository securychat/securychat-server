/*
This file is part of SecuryChat.
Copyright (C) 2024 Laljaka

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package encryption

import (
	"runtime"

	"golang.org/x/crypto/argon2"
	"golang.org/x/crypto/blake2b"
)

func GenerateKeyFromPassword(username, password string) (key *[32]byte) {
	cpus := runtime.NumCPU()
	if cpus > 1 {
		cpus = cpus - 1
	}

	k := argon2.IDKey([]byte(password), []byte(username), 1, 128*1024, uint8(cpus), 32)

	key = new([32]byte)
	copy(key[:], k)
	zeroSlice(k[:])

	return
}

func MacWithPasswordKey(key *[32]byte, msgs ...[]byte) ([]byte, error) {
	mac, err := blake2b.New256(key[:])
	if err != nil {
		return nil, err
	}

	for _, msg := range msgs {
		mac.Write(msg)
	}

	return mac.Sum(nil), nil
}
