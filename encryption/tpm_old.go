/*
This file is part of SecuryChat.
Copyright (C) 2024 Laljaka

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package encryption

import (
	"crypto/ecdsa"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"log"
	"math/big"

	"github.com/google/go-tpm/tpm2"
	"github.com/google/go-tpm/tpm2/transport"
)

func tryECDSA() {
	var flushed bool = false
	rwr, err := openTPM("/dev/tpmrm0") //"/dev/tpmrm0")
	if err != nil {
		log.Fatalf("Failed to open TPM: %v", err)
	}
	defer rwr.Close()

	cmdPrimary := tpm2.CreatePrimary{
		PrimaryHandle: tpm2.TPMRHOwner,
		InPublic:      tpm2.New2B(tpm2.ECCSRKTemplate),
	}

	primaryKey, err := cmdPrimary.Execute(rwr)
	if err != nil {
		log.Fatalf("can't create primary %v", err)
	}

	defer func() {
		if !flushed {
			flushContextCmd := tpm2.FlushContext{
				FlushHandle: primaryKey.ObjectHandle,
			}
			_, _ = flushContextCmd.Execute(rwr)
		}
	}()

	log.Printf("primaryKey Name %s\n", hex.EncodeToString(primaryKey.Name.Buffer))
	log.Printf("primaryKey handle Value %d\n", cmdPrimary.PrimaryHandle.HandleValue())

	eccTemplate := tpm2.TPMTPublic{
		Type:    tpm2.TPMAlgECC,
		NameAlg: tpm2.TPMAlgSHA256,
		ObjectAttributes: tpm2.TPMAObject{
			SignEncrypt:         true,
			FixedTPM:            true,
			FixedParent:         true,
			SensitiveDataOrigin: true,
			UserWithAuth:        true,
		},
		AuthPolicy: tpm2.TPM2BDigest{},

		Parameters: tpm2.NewTPMUPublicParms(
			tpm2.TPMAlgECC,
			&tpm2.TPMSECCParms{
				CurveID: tpm2.TPMECCNistP256,
				Scheme: tpm2.TPMTECCScheme{
					Scheme: tpm2.TPMAlgECDSA,
					Details: tpm2.NewTPMUAsymScheme(
						tpm2.TPMAlgECDSA,
						&tpm2.TPMSSigSchemeECDSA{
							HashAlg: tpm2.TPMAlgSHA256,
						},
					),
				},
			},
		),
	}

	eccKeyCommand := tpm2.CreateLoaded{
		ParentHandle: tpm2.AuthHandle{
			Handle: primaryKey.ObjectHandle,
			Name:   primaryKey.Name,
			Auth:   tpm2.PasswordAuth(nil),
		},
		InPublic: tpm2.New2BTemplate(&eccTemplate),
	}
	eccKeyResponse, err := eccKeyCommand.Execute(rwr)
	if err != nil {
		log.Fatalf("can't create ecc %v", err)
	}

	log.Printf("ecckey Name %s\n", hex.EncodeToString(eccKeyResponse.Name.Buffer))
	log.Printf("ecckey parent handle Value %d\n", eccKeyCommand.ParentHandle.HandleValue())

	defer func() {
		if !flushed {
			flushContextCmd := tpm2.FlushContext{
				FlushHandle: eccKeyResponse.ObjectHandle,
			}
			_, _ = flushContextCmd.Execute(rwr)
		}
	}()

	log.Printf("======= generate test signature with RSA key ========")
	data := []byte("Test")
	digest := sha256.Sum256(data)

	persistenthandle := 0x81008101
	_, err = tpm2.EvictControl{
		Auth: tpm2.TPMRHOwner,
		ObjectHandle: tpm2.NamedHandle{
			Handle: eccKeyResponse.ObjectHandle,
			Name:   eccKeyResponse.Name,
		},
		PersistentHandle: tpm2.TPMIDHPersistent(persistenthandle),
	}.Execute(rwr)
	if err != nil {
		log.Fatalf("can't create rsa %v", err)
	}

	flushContextCmd := tpm2.FlushContext{
		FlushHandle: eccKeyResponse.ObjectHandle,
	}
	_, _ = flushContextCmd.Execute(rwr)

	flushed = true

	h := tpm2.TPMHandle(persistenthandle)

	log.Printf("persistent Name %s\n", hex.EncodeToString(tpm2.HandleName(h).Buffer))
	log.Printf("persistent handle Value %d\n", h.HandleValue())

	pub, err := tpm2.ReadPublic{
		ObjectHandle: h,
	}.Execute(rwr)
	if err != nil {
		log.Fatalf("can't create rsa %v", err)
	}

	log.Printf("Public loaded Name %s\n", hex.EncodeToString(pub.Name.Buffer))

	h2 := tpm2.TPMIDHObject(persistenthandle)

	log.Printf("persistent 2 Name %s\n", hex.EncodeToString(tpm2.HandleName(h2).Buffer))
	log.Printf("persistent 2 handle Value %d\n", h2.HandleValue())

	pub2, err := tpm2.ReadPublic{
		ObjectHandle: h2,
	}.Execute(rwr)
	if err != nil {
		log.Fatalf("can't create rsa %v", err)
	}

	log.Printf("Public 2 loaded Name %s\n", hex.EncodeToString(pub2.Name.Buffer))

	sign := tpm2.Sign{
		KeyHandle: tpm2.NamedHandle{
			Handle: h,
			Name:   pub.Name,
		},
		Digest: tpm2.TPM2BDigest{
			Buffer: digest[:],
		},
		InScheme: tpm2.TPMTSigScheme{
			Scheme: tpm2.TPMAlgECDSA,
			Details: tpm2.NewTPMUSigScheme(
				tpm2.TPMAlgECDSA,
				&tpm2.TPMSSchemeHash{
					HashAlg: tpm2.TPMAlgSHA256,
				},
			),
		},
		Validation: tpm2.TPMTTKHashCheck{
			Tag: tpm2.TPMSTHashCheck,
		},
	}

	rspSign, err := sign.Execute(rwr)
	if err != nil {
		log.Fatalf("Failed to Sign: %v", err)
	}

	outPub, err := eccKeyResponse.OutPublic.Contents()
	if err != nil {
		log.Fatalf("Failed to get rsa public: %v", err)
	}

	ecDetail, err := outPub.Parameters.ECCDetail()
	if err != nil {
		log.Fatalf("Failed to get rsa public: %v", err)
	}
	crv, err := ecDetail.CurveID.Curve()
	if err != nil {
		log.Fatalf("Failed to get rsa public: %v", err)
	}

	eccUnique, err := outPub.Unique.ECC()
	if err != nil {
		log.Fatalf("Failed to get ecc public key: %v", err)
	}

	pubKey := &ecdsa.PublicKey{
		Curve: crv,
		X:     big.NewInt(0).SetBytes(eccUnique.X.Buffer),
		Y:     big.NewInt(0).SetBytes(eccUnique.Y.Buffer),
	}

	ecsig, err := rspSign.Signature.Signature.ECDSA()
	if err != nil {
		log.Fatalf("Failed to get signature part: %v", err)
	}

	out := append(ecsig.SignatureR.Buffer, ecsig.SignatureS.Buffer...)
	log.Printf("raw signature: %v\n", base64.StdEncoding.EncodeToString(out))
	log.Printf("ecpub: x %v\n", pubKey)
	ok := ecdsa.Verify(pubKey, digest[:], big.NewInt(0).SetBytes(ecsig.SignatureR.Buffer), big.NewInt(0).SetBytes(ecsig.SignatureS.Buffer))
	if !ok {
		log.Fatalf("Failed to verify signature: %v", err)
	}
}

func applyPCROLD(rwr transport.TPM, sess tpm2.Session, pcrSelection tpm2.TPMLPCRSelection) (tpm2.TPMLPCRSelection, tpm2.TPM2BDigest, error) {
	pcrReadRsp, err := tpm2.PCRRead{
		PCRSelectionIn: pcrSelection,
	}.Execute(rwr)
	if err != nil {
		return tpm2.TPMLPCRSelection{}, tpm2.TPM2BDigest{}, err
	}

	var expectedVal []byte
	for _, digest := range pcrReadRsp.PCRValues.Digests {
		expectedVal = append(expectedVal, digest.Buffer...)
	}

	cryptoHashAlg, err := tpm2.TPMAlgSHA256.Hash()
	if err != nil {
		return tpm2.TPMLPCRSelection{}, tpm2.TPM2BDigest{}, err
	}

	hash := cryptoHashAlg.New()
	hash.Write(expectedVal)
	digest := hash.Sum(nil)

	pcrSelectionSegment := tpm2.Marshal(pcrSelection)
	pcrDigestSegment := tpm2.Marshal(tpm2.TPM2BDigest{
		Buffer: digest,
	})

	commandParameter := append(pcrDigestSegment, pcrSelectionSegment...)
	// 0020e2f61c3f71d1defd3fa999dfa36953755c690689799962b48bebd836974e8cf900000001000b03000080
	fmt.Printf("pcrSelectionSegment %s\n", hex.EncodeToString(pcrSelectionSegment))
	fmt.Printf("pcrDigestSegment %s\n", hex.EncodeToString(pcrDigestSegment))
	fmt.Printf("commandParameter %s\n", hex.EncodeToString(commandParameter))

	pc, err := tpm2.Unmarshal[tpm2.TPMLPCRSelection](pcrSelectionSegment)
	if err != nil {
		return tpm2.TPMLPCRSelection{}, tpm2.TPM2BDigest{}, err
	}

	pdig, err := tpm2.Unmarshal[tpm2.TPM2BDigest](pcrDigestSegment)
	if err != nil {
		return tpm2.TPMLPCRSelection{}, tpm2.TPM2BDigest{}, err
	}

	_, err = tpm2.PolicyPCR{
		PolicySession: sess.Handle(),
		PcrDigest:     *pdig,
		Pcrs:          *pc,
	}.Execute(rwr)
	if err != nil {
		return tpm2.TPMLPCRSelection{}, tpm2.TPM2BDigest{}, err
	}

	return *pc, *pdig, nil
}
