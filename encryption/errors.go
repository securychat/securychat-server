/*
This file is part of SecuryChat.
Copyright (C) 2024 Laljaka

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package encryption

type ErrNotImplementedYet struct {
	reason string
}

func (err ErrNotImplementedYet) Error() string {
	return "Not implemented yet, reason: " + err.reason
}

type TPMError struct {
	original error
	reason   string
}

func (e *TPMError) Error() string {
	return e.reason + " " + e.original.Error()
}

func (e *TPMError) Unwrap() error {
	return e.original
}

func NewTPMError(reason string, original error) error {
	return &TPMError{original, reason}
}
