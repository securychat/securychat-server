/*
This file is part of SecuryChat.
Copyright (C) 2024 Laljaka

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package encryption

import (
	"fmt"
	"log"
	"testing"

	leg "github.com/google/go-tpm/legacy/tpm2"
	"github.com/google/go-tpm/tpm2"
)

const tpm string = "sim" // sim

func TestInSim(t *testing.T) {
	rwr, err := openTPM(tpm)
	if err != nil {
		t.Fatal(err)
	}
	defer rwr.Close()

	t.Run("Create persistant RSA", func(t *testing.T) {
		sess, clear, err := startSession(rwr)
		if err != nil {
			t.Fatal(err)
		}
		defer clear()

		pcrSelection := tpm2.TPMLPCRSelection{
			PCRSelections: []tpm2.TPMSPCRSelection{
				{
					Hash:      tpm2.TPMAlgSHA1, // GET PCR CAPABILITIES BEFORE CHOOSING
					PCRSelect: tpm2.PCClientCompatible.PCRs(1, 2, 3, 4, 5, 6, 7, 10),
				},
			},
		}

		_, d, err := applyPCR(rwr, sess, pcrSelection)
		if err != nil {
			t.Fatal(err)
		}

		ll, err := getPolicyDigest(rwr, sess)
		if err != nil {
			t.Fatal(err)
		}
		log.Println(ll)

		h := tpm2.TPMHandle(0x81008101)

		err = tryRSA(rwr, h, sess, d)
		if err != nil {
			t.Fatal(err)
		}
	})

	t.Run("Sign with RSA from persistant", func(t *testing.T) {
		sess, clear, err := startSession(rwr)
		if err != nil {
			t.Fatal(err)
		}
		defer clear()

		ll, err := getPolicyDigest(rwr, sess)
		if err != nil {
			t.Fatal(err)
		}
		log.Println(ll)

		pcrSelection := tpm2.TPMLPCRSelection{
			PCRSelections: []tpm2.TPMSPCRSelection{
				{
					Hash:      tpm2.TPMAlgSHA1, // GET PCR CAPABILITIES BEFORE CHOOSING
					PCRSelect: tpm2.PCClientCompatible.PCRs(1, 2, 3, 4, 5, 6, 7, 10),
				},
			},
		}

		ll, err = getPCR(rwr, pcrSelection)
		if err != nil {
			t.Fatal(err)
		}
		log.Println(ll)

		_, _, err = applyPCR(rwr, sess, pcrSelection)
		if err != nil {
			t.Fatal(err)
		}

		ll, err = getPolicyDigest(rwr, sess)
		if err != nil {
			t.Fatal(err)
		}
		log.Println(ll)

		sign := []byte("Test")

		sig, err := signWithRsaPersistant(rwr, sess, 0x81008101, sign)
		if err != nil {
			t.Fatal(err)
		}

		res, err := tpm2.ReadPublic{
			ObjectHandle: tpm2.TPMHandle(0x81008101),
		}.Execute(rwr)
		if err != nil {
			t.Fatal(err)
		}

		pub, err := res.OutPublic.Contents()
		if err != nil {
			t.Fatal(err)
		}

		err = checkRSASign(sign, pub, sig)
		if err != nil {
			t.Fatal(err)
		}
	})

	t.Run("Remove persistant", func(t *testing.T) {
		pub, err := tpm2.ReadPublic{
			ObjectHandle: tpm2.TPMHandle(0x81008101),
		}.Execute(rwr)
		if err != nil {
			t.Fatal(err)
		}

		_, err = tpm2.EvictControl{
			Auth: tpm2.TPMRHOwner,
			ObjectHandle: tpm2.NamedHandle{
				Name:   pub.Name,
				Handle: tpm2.TPMHandle(0x81008101),
			},
			PersistentHandle: tpm2.TPMHandle(0x81008101),
		}.Execute(rwr)
		if err != nil {
			t.Fatal(err)
		}
	})

}

func TestGetHandles(t *testing.T) {
	rwr, err := openTPM("/dev/tpmrm0")
	if err != nil {
		t.Fatal(err)
	}
	defer rwr.Close()

	c, err := getHandleCapabilities(rwr, uint32(leg.HandleTypePersistent))
	if err != nil {
		t.Fatal(err)
	}
	for _, hand := range c.Handle {
		t.Logf("%x", hand.HandleValue())
	}
}

func TestEvictControl(t *testing.T) {
	tp := TPMAccesPoint{
		device: "/dev/tpmrm0",
		handle: 0x810000bf,
	}

	err := tp.RemovePersistant()
	if err != nil {
		t.Fatal(err)
	}
}

func TestXxx(t *testing.T) {
	rwr, err := openTPM("sim")
	if err != nil {
		t.Fatal(err)
	}
	defer rwr.Close()

	resp, err := tpm2.GetCapability{
		Capability:    tpm2.TPMCapTPMProperties,
		Property:      293,
		PropertyCount: 100,
	}.Execute(rwr)
	if err != nil {
		t.Fatal(err)
	}

	props, err := resp.CapabilityData.Data.TPMProperties()
	if err != nil {
		t.Fatal(err)
	}

	for _, pr := range props.TPMProperty {
		fmt.Println("Property: ", pr.Property, "Value: ", pr.Value)
	}
}
