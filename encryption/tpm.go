/*
This file is part of SecuryChat.
Copyright (C) 2024 Laljaka

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package encryption

import (
	"crypto"
	"crypto/rsa"
	"crypto/x509"
	"errors"
	"fmt"
	"io"
	"math/rand/v2"
	"os"
	"runtime"

	"github.com/google/go-tpm/tpm2"
	"github.com/google/go-tpm/tpm2/transport"
	"github.com/google/go-tpm/tpmutil"
)

const (
	lowerBound uint32 = 0x81000001
)

type TPMAccesPoint struct {
	device string
	handle tpm2.TPMHandle
	pcrs   tpm2.TPMLPCRSelection
}

func NewTPMAccesPoint(device string) *TPMAccesPoint {
	return &TPMAccesPoint{
		device: device,
		pcrs:   createPCRSelection(1, 2, 3, 4, 5, 6, 7, 10),
	}
}

func GetTPMDevice() (string, error) {
	device, err := getTPMDevice()
	if err != nil {
		return "", err
	}
	return device, nil
}

func (t *TPMAccesPoint) SetHandle(h uint32) {
	t.handle = tpm2.TPMHandle(h)
}

func GeneratePersistantHandle(device string) (uint32, error) {
	rwr, err := openTPM(device)
	if err != nil {
		return 0, NewTPMError("failed to open TPM device", err)
	}
	defer rwr.Close()

	handles, err := getHandleCapabilities(rwr, 0x81)
	if err != nil {
		return 0, NewTPMError("failed to retrieve persistent handles", err)
	}

	props, err := getPropertyCapabilities(rwr, 293)
	if err != nil {
		return 0, NewTPMError("failed to retrieve TPM property capabilities", err)
	}

	upper := props.TPMProperty[0].Value

	newHandles := make(map[uint32]bool)
	for _, h := range handles.Handle {
		newHandles[h.HandleValue()] = true
	}

	var h uint32
	for {
		h = generateRandomHandle(upper)
		if _, exists := newHandles[h]; !exists {
			break
		}
	}

	sess, close, err := startSession(rwr)
	if err != nil {
		return 0, NewTPMError("failed to start session", err)
	}
	defer close()

	_, digest, err := applyPCR(rwr, sess, createPCRSelection(1, 2, 3, 4, 5, 6, 7, 10))
	if err != nil {
		return 0, NewTPMError("failed to apply PCR selection", err)
	}

	fmt.Println(h)

	err = tryRSA(rwr, tpm2.TPMHandle(h), sess, digest)
	if err != nil {
		return 0, NewTPMError("failed to create or store RSA key in persistent handle", err)
	}

	return h, nil
}

func createPCRSelection(pcrs ...uint) tpm2.TPMLPCRSelection {
	return tpm2.TPMLPCRSelection{
		PCRSelections: []tpm2.TPMSPCRSelection{
			{
				Hash:      tpm2.TPMAlgSHA1, // GET PCR CAPABILITIES BEFORE CHOOSING
				PCRSelect: tpm2.PCClientCompatible.PCRs(pcrs...),
			},
		},
	}
}

func getHandleCapabilities(rwr transport.TPMCloser, prop uint32) (*tpm2.TPMLHandle, error) {
	property := prop << 24
	resp, err := tpm2.GetCapability{
		Capability:    tpm2.TPMCapHandles,
		Property:      property,
		PropertyCount: 100,
	}.Execute(rwr)
	if err != nil {
		return nil, NewTPMError("failed to retrieve TPM handle capability", err)
	}

	hands, err := resp.CapabilityData.Data.Handles()
	if err != nil {
		return nil, NewTPMError("failed to parse TPM handle capability response", err)
	}

	return hands, nil
}

func getPropertyCapabilities(rwr transport.TPMCloser, prop uint32) (*tpm2.TPMLTaggedTPMProperty, error) {
	resp, err := tpm2.GetCapability{
		Capability:    tpm2.TPMCapTPMProperties,
		Property:      prop,
		PropertyCount: 1,
	}.Execute(rwr)
	if err != nil {
		return nil, NewTPMError("failed to retrieve TPM property capability", err)
	}

	props, err := resp.CapabilityData.Data.TPMProperties()
	if err != nil {
		return nil, NewTPMError("failed to parse TPM property capability response", err)
	}

	return props, nil
}

func openTPM(path string) (transport.TPMCloser, error) {
	var closer io.ReadWriteCloser
	var err error

	closer, err = tpmutil.OpenTPM(path)

	if err != nil {
		return nil, err
	} else {
		return transport.FromReadWriteCloser(closer), nil
	}
}

func getTPMDevice() (string, error) {
	switch runtime.GOOS {
	case "linux":
		_, err := os.Stat("/dev/tpmrm0") //"/dev/tpm0"
		if err != nil {
			_, err = os.Stat("/dev/tpm0")
			if err != nil {
				return "", errors.New("no device found")
			}
			return "/dev/tpm0", nil
		}
		return "/dev/tpmrm0", nil

	case "windows":
		return "", &ErrNotImplementedYet{"cgo and windows required"}
	case "darwin":
		return "", errors.New("unsupported operating system")
	default:
		return "", errors.New("unsupported operating system")
	}
}

func generateRandomHandle(max uint32) uint32 {
	upper := lowerBound + max
	var h = rand.Uint32()
	h %= (upper - 1 - lowerBound) //upperBound - lowerBound)
	h += lowerBound
	return h
}

func tryRSA(rwr transport.TPM, handle tpm2.TPMHandle, sess tpm2.Session, digest tpm2.TPM2BDigest) error {
	primaryKey, err := tpm2.CreatePrimary{
		PrimaryHandle: tpm2.TPMRHOwner,
		InPublic:      tpm2.New2B(tpm2.RSASRKTemplate),
	}.Execute(rwr)
	if err != nil {
		return NewTPMError("failed to create primary RSA key", err)
	}
	defer tpm2.FlushContext{FlushHandle: primaryKey.ObjectHandle}.Execute(rwr)

	rsaTemplate := tpm2.TPMTPublic{
		Type:    tpm2.TPMAlgRSA,
		NameAlg: tpm2.TPMAlgSHA256,
		ObjectAttributes: tpm2.TPMAObject{
			SignEncrypt:         true,
			FixedTPM:            true,
			FixedParent:         true,
			SensitiveDataOrigin: true,
			UserWithAuth:        true,
		},
		AuthPolicy: digest,
		Parameters: tpm2.NewTPMUPublicParms(
			tpm2.TPMAlgRSA,
			&tpm2.TPMSRSAParms{
				Scheme: tpm2.TPMTRSAScheme{
					Scheme: tpm2.TPMAlgRSASSA,
					Details: tpm2.NewTPMUAsymScheme(
						tpm2.TPMAlgRSASSA,
						&tpm2.TPMSSigSchemeRSASSA{
							HashAlg: tpm2.TPMAlgSHA256,
						},
					),
				},
				KeyBits: 2048,
			},
		),
	}

	rsaKey, err := tpm2.Create{
		ParentHandle: tpm2.AuthHandle{
			Handle: primaryKey.ObjectHandle,
			Name:   primaryKey.Name,
			Auth:   tpm2.PasswordAuth(nil),
		},
		//CreationPCR: createPCRSelection(1, 2, 3, 4, 5, 6, 7, 10),
		InPublic: tpm2.New2B(rsaTemplate),
	}.Execute(rwr)
	if err != nil {
		return NewTPMError("failed to create RSA key under primary", err)
	}

	rsaKeyResponse, err := tpm2.Load{
		ParentHandle: tpm2.AuthHandle{
			Handle: primaryKey.ObjectHandle,
			Name:   primaryKey.Name,
			Auth:   tpm2.PasswordAuth(nil),
		},
		InPrivate: rsaKey.OutPrivate,
		InPublic:  rsaKey.OutPublic,
	}.Execute(rwr)
	if err != nil {
		return NewTPMError("failed to load RSA key into TPM", err)
	}

	//zeroMemory(rsaKey)
	//rsaKey = nil

	defer tpm2.FlushContext{FlushHandle: rsaKeyResponse.ObjectHandle}.Execute(rwr)

	_, err = tpm2.EvictControl{
		Auth: tpm2.TPMRHOwner,
		ObjectHandle: tpm2.NamedHandle{
			Handle: rsaKeyResponse.ObjectHandle,
			Name:   rsaKeyResponse.Name,
		},
		PersistentHandle: handle,
	}.Execute(rwr)
	if err != nil {
		return NewTPMError("failed to make RSA key persistent", err)
	}

	fmt.Printf("Using persistent handle: 0x%X\n", handle)
	return nil
}

func signWithRsaPersistant(rwr transport.TPM, session tpm2.Session, persistent tpm2.TPMHandle, toSign []byte) (*tpm2.TPMSSignatureRSA, error) {
	res, err := tpm2.ReadPublic{
		ObjectHandle: persistent,
	}.Execute(rwr)
	if err != nil {
		return nil, NewTPMError("failed to read public area of persistent key", err)
	}

	digest, err := tpm2.Hash{
		Hierarchy: tpm2.TPMRHOwner,
		HashAlg:   tpm2.TPMAlgSHA256,
		Data: tpm2.TPM2BMaxBuffer{
			Buffer: toSign,
		},
	}.Execute(rwr)
	if err != nil {
		return nil, NewTPMError("failed to compute hash for signing", err)
	}

	rspSign, err := tpm2.Sign{
		KeyHandle: tpm2.AuthHandle{
			Handle: persistent,
			Name:   res.Name,
			Auth:   session,
		},
		Digest: digest.OutHash,
		InScheme: tpm2.TPMTSigScheme{
			Scheme: tpm2.TPMAlgRSASSA,
			Details: tpm2.NewTPMUSigScheme(
				tpm2.TPMAlgRSASSA,
				&tpm2.TPMSSchemeHash{
					HashAlg: tpm2.TPMAlgSHA256,
				},
			),
		},
		Validation: tpm2.TPMTTKHashCheck{
			Tag:       tpm2.TPMSTHashCheck,
			Hierarchy: tpm2.TPMRHOwner,
			Digest:    digest.Validation.Digest,
		},
	}.Execute(rwr)
	if err != nil {
		return nil, NewTPMError("failed to sign digest with persistent key", err)
	}

	rsassa, err := rspSign.Signature.Signature.RSASSA()
	if err != nil {
		return nil, NewTPMError("failed to extract RSASSA signature from TPM response", err)
	}

	return rsassa, nil
}

func checkRSASign(toCheck []byte, pub *tpm2.TPMTPublic, sign *tpm2.TPMSSignatureRSA) error {
	akhsh := crypto.SHA256.New()
	akhsh.Write(toCheck)

	rsaDetail, err := pub.Parameters.RSADetail()
	if err != nil {
		return err
	}
	rsaUnique, err := pub.Unique.RSA()
	if err != nil {
		return err
	}

	rsaPub, err := tpm2.RSAPub(rsaDetail, rsaUnique)
	if err != nil {
		return err
	}

	if err := rsa.VerifyPKCS1v15(rsaPub, crypto.SHA256, akhsh.Sum(nil), sign.Sig.Buffer); err != nil {
		return errors.Join(errors.New("could not verify"), err)
	}

	return nil
}

func startSession(rwr transport.TPM) (tpm2.Session, func() error, error) {
	sess, clean, err := tpm2.PolicySession(rwr, tpm2.TPMAlgSHA256, 16)
	if err != nil {
		return nil, nil, err
	}

	return sess, clean, nil
}

func applyPCR(rwr transport.TPM, sess tpm2.Session, pcrSelection tpm2.TPMLPCRSelection) (tpm2.TPMLPCRSelection, tpm2.TPM2BDigest, error) {
	_, err := tpm2.PolicyPCR{
		PolicySession: sess.Handle(),
		Pcrs:          pcrSelection,
	}.Execute(rwr)
	if err != nil {
		return tpm2.TPMLPCRSelection{}, tpm2.TPM2BDigest{}, err
	}

	pgd, err := tpm2.PolicyGetDigest{
		PolicySession: sess.Handle(),
	}.Execute(rwr)
	if err != nil {
		return tpm2.TPMLPCRSelection{}, tpm2.TPM2BDigest{}, err
	}

	return pcrSelection, pgd.PolicyDigest, nil
}

func getPolicyDigest(rwr transport.TPM, sess tpm2.Session) (string, error) {
	policyDigestResp, err := tpm2.PolicyGetDigest{
		PolicySession: sess.Handle(),
	}.Execute(rwr)
	if err != nil {
		return "", err
	}

	policyDigest := policyDigestResp.PolicyDigest.Buffer

	return fmt.Sprintf("Policy Digest: %x\n", policyDigest), nil
}

func getPCR(rwr transport.TPM, pcrs tpm2.TPMLPCRSelection) (string, error) {
	pcrReadRsp, err := tpm2.PCRRead{
		PCRSelectionIn: pcrs,
	}.Execute(rwr)
	if err != nil {
		return "", err
	}

	result := "PCR values:\n"
	for i, pcr := range pcrReadRsp.PCRValues.Digests {
		result = fmt.Sprintf(result+"PCR %d digest: %x\n", i, pcr.Buffer)
	}
	result = result + "\nWith PCR selection:\n"
	for i, pcr := range pcrReadRsp.PCRSelectionOut.PCRSelections {
		result = fmt.Sprintf(result+"PCR %d selection: %v", i, pcr)
	}

	return result, nil
}

func (t *TPMAccesPoint) GetPublicKey() ([]byte, error) {
	rwr, err := openTPM(t.device)
	if err != nil {
		return nil, NewTPMError("failed to open TPM device: ", err)
	}
	defer rwr.Close()

	fmt.Printf("Using persistent handle: 0x%X\n", t.handle)

	resp, err := tpm2.ReadPublic{
		ObjectHandle: t.handle,
	}.Execute(rwr)
	if err != nil {
		return nil, NewTPMError("failed to read public key from TPM: ", err)
	}

	cont, err := resp.OutPublic.Contents()
	if err != nil {
		return nil, NewTPMError("failed to extract public area contents: ", err)
	}

	rsaDetail, err := cont.Parameters.RSADetail()
	if err != nil {
		return nil, NewTPMError("failed to get RSA parameters from TPM public key: ", err)
	}

	rsaUnique, err := cont.Unique.RSA()
	if err != nil {
		return nil, NewTPMError("failed to extract RSA unique value from TPM public key: ", err)
	}

	rsaPub, err := tpm2.RSAPub(rsaDetail, rsaUnique)
	if err != nil {
		return nil, NewTPMError("failed to reconstruct RSA public key: ", err)
	}

	encoded, err := encodeRSA(rsaPub)
	if err != nil {
		return nil, NewTPMError("failed to encode RSA public key: ", err)
	}

	return encoded, nil
}

func encodeRSA(pubKey *rsa.PublicKey) ([]byte, error) {
	der, err := x509.MarshalPKIXPublicKey(pubKey)
	if err != nil {
		return nil, err
	}

	return der, nil
}

func (t *TPMAccesPoint) Sign(challenge []byte) ([]byte, error) {
	rwr, err := openTPM(t.device)
	if err != nil {
		return nil, NewTPMError("failed to open TPM device", err)
	}
	defer rwr.Close()

	sess, close, err := startSession(rwr)
	if err != nil {
		return nil, NewTPMError("failed to start TPM session", err)
	}
	defer close()

	_, _, err = applyPCR(rwr, sess, t.pcrs)
	if err != nil {
		return nil, NewTPMError("failed to apply PCR settings", err)
	}

	signed, err := signWithRsaPersistant(rwr, sess, t.handle, challenge)
	if err != nil {
		return nil, NewTPMError("failed to sign challenge with persistent key", err)
	}

	return signed.Sig.Buffer, nil
}

func (t *TPMAccesPoint) RemovePersistant() error {
	rwr, err := openTPM(t.device)
	if err != nil {
		return NewTPMError("failed to open TPM device", err)
	}
	defer rwr.Close()

	pub, err := tpm2.ReadPublic{
		ObjectHandle: t.handle,
	}.Execute(rwr)
	if err != nil {
		return NewTPMError("failed to read public area of TPM object", err)
	}

	_, err = tpm2.EvictControl{
		Auth: tpm2.TPMRHOwner,
		ObjectHandle: tpm2.NamedHandle{
			Name:   pub.Name,
			Handle: t.handle,
		},
		PersistentHandle: t.handle,
	}.Execute(rwr)
	if err != nil {
		return NewTPMError("failed to evict persistent TPM handle", err)
	}

	return nil
}
