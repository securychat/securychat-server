/*
This file is part of SecuryChat.
Copyright (C) 2024 Laljaka

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package encryption

import (
	"crypto/rand"
	"io"

	"golang.org/x/crypto/blake2b"
	"golang.org/x/crypto/curve25519"
)

func GenerateKeyPair() ([]byte, []byte, error) {
	privateKey := make([]byte, 32)
	_, err := io.ReadFull(rand.Reader, privateKey)
	if err != nil {
		zeroSlice(privateKey)
		return nil, nil, err
	}

	pub, err := curve25519.X25519(privateKey, curve25519.Basepoint)
	if err != nil {
		zeroSlice(privateKey)
		return nil, nil, err
	}

	return privateKey, pub, nil
}

func SessionKeysAsServer(serverPrivateKey, serverPublicKey, clientPublicKey []byte) (receiveKey, sendKey []byte, err error) {
	sharedSecret, err := deriveSharedSecret(serverPrivateKey, clientPublicKey)
	if err != nil {
		return
	}
	defer zeroSlice(sharedSecret)
	defer zeroSlice(serverPrivateKey)
	key1, key2, err := deriveSessionKeys(sharedSecret, clientPublicKey, serverPublicKey)
	return key1, key2, err
}

func SessionKeysAsClient(clientPrivateKey, clientPublicKey, serverPublicKey []byte) (receiveKey, sendKey []byte, err error) {
	sharedSecret, err := deriveSharedSecret(clientPrivateKey, serverPublicKey)
	if err != nil {
		return
	}
	defer zeroSlice(sharedSecret)
	defer zeroSlice(clientPrivateKey)
	key1, key2, err := deriveSessionKeys(sharedSecret, clientPublicKey, serverPublicKey)
	return key2, key1, err
}

func deriveSharedSecret(privateKey, otherPublicKey []byte) ([]byte, error) {
	bytes, err := curve25519.X25519(privateKey, otherPublicKey)
	if err != nil {
		return nil, err
	}

	return bytes, nil
}

func deriveSessionKeys(sharedSecret, clientPK, serverPK []byte) (rx, tx []byte, err error) {
	rx = make([]byte, 32)
	tx = make([]byte, 32)

	hash, err := blake2b.New512(nil)
	if err != nil {
		return nil, nil, err
	}

	//first, second := orderKeys(clientPK, serverPK)

	hash.Write(sharedSecret)
	hash.Write(clientPK)
	hash.Write(serverPK)

	final := hash.Sum(nil)

	copy(rx, final[:32])
	copy(tx, final[32:])
	zeroSlice(final)
	return
}
