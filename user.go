/*
This file is part of SecuryChat.
Copyright (C) 2024 Laljaka

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package main

import (
	"fmt"
	"iter"
	"sync"

	"gitlab.com/securychat/securychat-server/db"
	"gitlab.com/securychat/securychat-server/linked_list"
	"gitlab.com/securychat/securychat-server/protocol"
)

type SendFunc func(protocol.Payload)

type MessageUser struct {
	me      db.UserID
	mu      sync.RWMutex
	list    linked_list.LinkedList[protocol.Payload]
	recv    SendFunc
	allowed map[db.ChatID]*Chat
}

func (u *MessageUser) AddChat(c db.ChatID, chat *Chat) {
	u.mu.Lock()
	defer u.mu.Unlock()
	u.allowed[c] = chat
}

func (u *MessageUser) RemoveChat(c db.ChatID) {
	u.mu.Lock()
	defer u.mu.Unlock()
	delete(u.allowed, c)
}

func (u *MessageUser) SendTo(c db.ChatID, m protocol.Payload) {
	u.mu.RLock()
	fmt.Println("sending to", u.allowed, u.me)
	u.allowed[c].Publish(u.me, m)
	u.mu.RUnlock()
}

func (u *MessageUser) Online(ch chan protocol.Payload) {
	u.mu.Lock()
	defer u.mu.Unlock()
	u.recv = func(m protocol.Payload) {
		ch <- m
	}
}

func (u *MessageUser) Offline() {
	u.mu.Lock()
	defer u.mu.Unlock()
	u.recv = func(m protocol.Payload) {
		u.list.Append(m)
	}
}

func (u *MessageUser) GetMsgs() iter.Seq[protocol.Payload] {
	u.mu.RLock()
	defer u.mu.RUnlock()
	return func(yield func(protocol.Payload) bool) {
		item, ok := u.list.GrabFirst()
		if ok {
			yield(item)
		}
	}
}

func (u *MessageUser) Send(msg protocol.Payload) {
	u.mu.RLock()
	u.recv(msg)
	u.mu.RUnlock()
}
