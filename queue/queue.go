/*
This file is part of SecuryChat.
Copyright (C) 2024 Laljaka

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
*/

package queue

import "sync"

type Queue[T any] struct {
	mu sync.Mutex
	uq []T
}

func (q *Queue[T]) Enqueue(msg T) {
	q.mu.Lock()
	defer q.mu.Unlock()
	q.uq = append(q.uq, msg)
}

func (q *Queue[T]) Dequeue() (T, bool) {
	q.mu.Lock()
	defer q.mu.Unlock()

	if len(q.uq) == 0 {
		var zero T
		return zero, false
	}

	first := q.uq[0]
	q.uq = q.uq[1:]
	return first, true
}
